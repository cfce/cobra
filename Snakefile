configfile: "config.yaml"


# Check metasheet setup
from scripts.metasheet_setup import updateMeta
import pandas as pd
import yaml

config = updateMeta(config)
metadata = pd.read_table(config['metasheet'], index_col=0, sep=',', comment='#')

# Get comparsion columns for DEseq2
def _getColumn(comparison):
    return metadata["comp_{}".format(comparison)]

def _getComparison(name, group):
    comp = _getColumn(name)
    return metadata[comp == group].index

def _getSamples(wildcards):
    comp = _getColumn(wildcards.comparison)
    return comp.dropna().index

def loadRef(config):
    """Adds the static reference paths found in config['ref']
    NOTE: if the elm is already defined, then we DO NOT clobber the value
    """
    f = open(config['ref'])
    ref_info = yaml.safe_load(f)
    f.close()
    #print(ref_info[config['assembly']])
    for (k,v) in ref_info[config['assembly']].items():
        #NO CLOBBERING what is user-defined!
        if k not in config:
            config[k] = v

#NOW load ref.yaml - SIDE-EFFECT: loadRef CHANGES config
loadRef(config)


from string import Template

def getRuns(config):
    """parse metasheet for Run groupings"""
    ret = {}

    #LEN: Weird, but using pandas to handle the comments in the file
    #KEY: need skipinitialspace to make it fault tolerant to spaces!
    metadata = pd.read_table(config['metasheet'], index_col=0, sep=',', comment='#', skipinitialspace=True)
    f = metadata.to_csv().split() #make it resemble an actual file with lines
    #SKIP the hdr
    for l in f[1:]:
        tmp = l.strip().split(",")
        #print(tmp)
        ret[tmp[0]] = tmp[1:]

    #print(ret)
    config['runs'] = ret
    return config


config = getRuns(config)

# Param strings for plot path
params = "rpkm." + str(config["rpkm_threshold"]) + "_num_sample." + str(config["mini_num_sample"])  + "_scale." + config["scale"] + "_fliter." + config["filter-opt"] + "." + str(config["filter-percent"])

# Param strings for plot name
SSparams = "_" + str(config["filter-percent"]) +  "_percent"
SFparams = "_" + str(config["filter-percent"]) +  "_percent"

# Param strings for DEseq2
DEseq_up="Padj" + str(config["Padj"]) + ".LG2FC." + str(config["LG2FC"])
DEseq_down="Padj" + str(config["Padj"]) + ".LG2FC.-" + str(config["LG2FC"])

# Param strings for reference files
reference = str(config["assembly"])


rule download_example_GR_ChIP_fastq:
    params:
        GR = "https://www.dropbox.com/s/0bszyi7b1fe2d92/GR_ChIP_test_data_fastq.tar.gz?dl=0",
    output:
        "fastq/0.5nM_Dex_1.fastq.gz"
    shell:
        "wget {params.GR} -O GR_ChIP_test_data.tar.gz && tar xvzf GR_ChIP_test_data.tar.gz "


rule download_example_GR_ChIP:
    params:
        GR = "https://www.dropbox.com/s/nubhqly4hw4ez7o/GR_ChIP_test_data.tar.gz?dl=0",
    output:
        "bam_bed_bw/bam/0.5nM_Dex_1.bam"
    shell:
        "wget {params.GR} -O GR_ChIP_test_data.tar.gz && tar xvzf GR_ChIP_test_data.tar.gz "


rule download_example_MSS_MSI:
    params:
        MSS = "https://www.dropbox.com/s/i44n14208vg44tg/MSS_MSI_ChIP_test_data.tar.gz?dl=0",
    output:
        "bam_bed_bw/bam/H3K27ac_HCT-116_Rep1.bam"
    shell:
        "wget {params.MSS} -O MSS_MSI_ChIP_test_data.tar.gz && tar xvzf MSS_MSI_ChIP_test_data.tar.gz "

rule download_example_Macrophage_atac:
    params:
        atac = "https://www.dropbox.com/s/67ecsn9i7hvvh7c/Macrophage_atac_seq_test_data.tar.gz?dl=0",
    output:
        "bam_bed_bw/bam/HL60_Rep_1.bam"
    shell:
        "wget {params.atac} -O Macrophage_atac_seq_test_data.tar.gz && tar xvzf Macrophage_atac_seq_test_data.tar.gz "


rule bwa_index:
    params:
        bwa_index = config["bwa_index"],
    output:
        bwa_index = expand("ref_files/{ref}/bwa_indices/{ref}/{ref}.fa.bwt", ref=reference),
        bwa_path = expand("ref_files/{ref}", ref=reference)
    shell:
        "wget {params.bwa_index} -O {output.bwa_path}/bwa_index.tar.gz && cd {output.bwa_path} && tar -zxvf bwa_index.tar.gz "


rule chrom_lens:
    params:
        chrom_lens = config["chrom_lens"],
    output:
        chrom_lens = expand("ref_files/{ref}/{ref}.len", ref=reference),
    shell:
        "wget {params.chrom_lens} -O {output.chrom_lens}"


rule get_ref_genome:
    params:
        genome=config["genome"],
        genome_dict=config["genome_dict"],
    output:
        genome = expand("ref_files/{ref}/genome.fasta", ref=reference),
        genome_dict = expand("ref_files/{ref}/genome.dict",ref=reference)
    shell:
        "wget {params.genome} -O {output.genome} && wget {params.genome_dict} -O {output.genome_dict} "


rule get_ref_gene:
    params:
        TSS_plus_minus_1kb = config["TSS.plus.minus.1kb"],
        refseqGenes = config["refseqGenes"],
    output:
        TSS_plus_minus_1kb = expand("ref_files/{ref}/refseq_{ref}_TSS.sort.minus.plus.1kb.bed", ref=reference),
        refseqGenes = expand("ref_files/{ref}/refGene.{ref}.id.bed",ref=reference)
    shell:
        "wget {params.TSS_plus_minus_1kb} -O {output.TSS_plus_minus_1kb} && wget {params.refseqGenes} -O {output.refseqGenes} "


rule get_lift_chain:
    params:
        lift_chain = config["lift.chain"],
    output:
        lift_chain = expand("ref_files/liftover/{ref}.lift.chain.gz", ref=reference),
    shell:
        "wget {params.lift_chain} -O {output.lift_chain}"

rule get_giggle:
    params:
        giggle = config["giggle"],
    output:
        giggle = "ref_files/giggle/CistromeDB.sample.annotation.txt"
    shell:
        "wget {params.giggle} -O ref_files/giggle.tar.gz && cd ref_files && tar -zxvf giggle.tar.gz "



if config['fastq_in'] == 'true':
    include: "./scripts/snakefile.align.macs2.bg2bw"     # rules specific to sort bam files
else:
    include: "./scripts/snakefile.bed.bigwig"      # rules specific to copy bam



rule make_concat_bed:
    input:
        lambda wildcards: expand("analysis/preprocessed_files/peaks/{run}.rep1/{run}.rep1_peaks.narrowPeak", run=config['runs'])
    output:
        "analysis/preprocessed_files/bed/concatbed/concat_all.bed"
    shell:
        "cat {input} > {output}"


rule sort_concat_bed:
    input:
        "analysis/preprocessed_files/bed/concatbed/concat_all.bed"
    output:
        "analysis/preprocessed_files/bed/concatbed/concat_all_sort.bed"
    shell:
        "sort -k1,1 -k2,2g -o {output} {input}"

rule cut_concat_bed_columns:
    input:
        "analysis/preprocessed_files/bed/concatbed/concat_all_sort.bed"
    output:
        "analysis/preprocessed_files/bed/concatbed/concat_all_sort_cut.bed"
    shell:
        "cut -f -3 {input} > {output}"

rule merged_bed:
    input:
        "analysis/preprocessed_files/bed/concatbed/concat_all_sort_cut.bed"
    output:
        "analysis/preprocessed_files/bed/concatbed/concat_all_sort_cut_merged.bed"
    shell:
        "bedtools merge -i {input} > {output}"

rule add_id_column:
    input:
        "analysis/preprocessed_files/bed/concatbed/concat_all_sort_cut_merged.bed"
    output:
        "analysis/preprocessed_files/bed/concatbed/concat_all_sort_cut_merged_id.bed"
    shell:
        """
        awk '{{print $1"\t"$2"\t"$3"\t"$1"_"$2"_"$3}}' {input} > {output}
        """


if config['bam_sort'] == 'true' and config['fastq_in'] == 'false' :
    include: "./scripts/snakefile.copy.bam"     # rules specific to sort bam files
elif config['bam_sort'] == 'false' and config['fastq_in'] == 'false' :
    include: "./scripts/snakefile.sort.bam"      # rules specific to copy bam


rule bed_enhancer_promoter:
    input:
        bed="analysis/preprocessed_files/bed/concatbed/concat_all_sort_cut_merged_id.bed",
        TSS_plus_minus_1kb = expand("ref_files/{ref}/refseq_{ref}_TSS.sort.minus.plus.1kb.bed", ref=reference),
    output:
        enhancer="analysis/preprocessed_files/bed/enhancer_peaks_sort.bed",
        promoter="analysis/preprocessed_files/bed/promoter_peaks_sort.bed",
        all="analysis/preprocessed_files/bed/all_sort.bed"
    shell:
        "bedtools intersect -wa -a {input.bed} -b {input.TSS_plus_minus_1kb} -v > analysis/preprocessed_files/bed/enhancer_peaks.bed"
        " && bedtools intersect -wa -a {input.bed} -b {input.TSS_plus_minus_1kb} -u > analysis/preprocessed_files/bed/promoter_peaks.bed"
        " && python2.7 scripts/sortBed.py -f analysis/preprocessed_files/bed/enhancer_peaks.bed -a > {output.enhancer}"
        " && python2.7 scripts/sortBed.py -f analysis/preprocessed_files/bed/promoter_peaks.bed -a > {output.promoter}"
        " && python2.7 scripts/sortBed.py -f {input.bed} -a > {output.all}"


rule multi_bam_summary:
    input:
        bed="analysis/preprocessed_files/bed/all_sort.bed",
        enhancer="analysis/preprocessed_files/bed/enhancer_peaks_sort.bed",
        promoter="analysis/preprocessed_files/bed/promoter_peaks_sort.bed",
        bam="analysis/preprocessed_files/align/{sample}/{sample}.sorted.bam"
    output:
        tsv="analysis/preprocessed_files/read_counts/sample_counts/{sample}.tsv",
        npz="analysis/preprocessed_files/read_counts/sample_counts/{sample}.npz",
        total_count="analysis/preprocessed_files/read_counts/sample_counts/{sample}.total_count"    # no change
    params:
        enhancer=config["enhancer"]
    log:
        "analysis/logs/read_coutns/{sample}.log"
    shell:
        """
        samtools flagstat {input.bam} > {output.total_count} &&
        samtools index {input.bam} &&
        if [ "{params.enhancer}" == "enhancer" ]; then  (multiBamSummary BED-file --BED {input.enhancer} --bamfiles {input.bam} --outRawCounts {output.tsv} -out {output.npz}) 2>{log}
        elif [ "{params.enhancer}" == "promoter" ]; then  (multiBamSummary BED-file --BED {input.promoter} --bamfiles {input.bam} --outRawCounts {output.tsv} -out {output.npz}) 2>{log}
        else  (multiBamSummary BED-file --BED {input.bed} --bamfiles {input.bam} --outRawCounts {output.tsv} -out {output.npz}) 2>{log}
        fi
        """

rule convert_tsv_to_count:
    input:
        "analysis/preprocessed_files/read_counts/sample_counts/{sample}.tsv"
    output:
        "analysis/preprocessed_files/read_counts/sample_counts/{sample}.count"
    shell:
        """
        sed '1d' {input} | awk '{{print $1"\t"$2"\t"$3"\t"$1"_"$2"_"$3"\t"$4}}' > {output}
        """

rule map_stats:
    """Get the mapping stats for each aligment run"""
    input:
        bam="analysis/preprocessed_files/align/{sample}/{sample}.sorted.bam",
    output:
        "analysis/preprocessed_files/align/{sample}/{sample}_mapping.txt"
    message: "ALIGN: get mapping stats for each bam"
    shell:
        #FLAGSTATS is the top of the file, and we append the uniquely mapped
        #reads to the end of the file
        "samtools flagstat {input.bam} > {output}"

rule collect_map_stats:
    """Collect and parse out the mapping stats for the ALL of the samples"""
    input:
        #samples sorted to match order of rest of report
        expand("analysis/preprocessed_files/align/{sample}/{sample}_mapping.txt", sample=sorted(config["runs"]))
    output:
        "analysis/preprocessed_files/align/mapping.csv"
    message: "ALIGN: collect and parse ALL mapping stats"
    run:
        files = " -f ".join(input)
        shell("scripts/align_getMapStats.py -f {files} > {output}")

rule getPeaksStats:
    input:
        #Generalized INPUT fn defined in chips.snakefile
        expand("analysis/preprocessed_files/peaks/{sample}.rep1/{sample}.rep1_peaks.narrowPeak", sample=sorted(config["runs"]))
    output:
        "analysis/preprocessed_files/peaks/peakStats.csv"
    message: "PEAKS: collecting peaks stats for each run"
    run:
        files = " -f ".join(input)
        shell("scripts/peaks_getPeakStats.py -f {files} -o {output}")

rule sequenceSummary:
    input:
        mapping = "analysis/preprocessed_files/align/mapping.csv",
        peakStat = "analysis/preprocessed_files/peaks/peakStats.csv",
    output:
        "analysis/report/sequenceSummary.csv"
    run:
        shell("Rscript scripts/sequenceSummary.R {input.mapping} {input.peakStat} {output}")

rule cnv_file:
    input:
        lambda wildcards: config["cnv"][wildcards.sample]
    output:
        protected("analysis/preprocessed_files/CNV/{sample}/{sample}.cnv")
    shell:
        "cp {input} {output}"


rule get_cnv:
    input:
        bed="analysis/preprocessed_files/bed/all_sort.bed",
        enhancer="analysis/preprocessed_files/bed/enhancer_peaks_sort.bed",
        promoter="analysis/preprocessed_files/bed/promoter_peaks_sort.bed",
        CNV="analysis/preprocessed_files/CNV/{sample}/{sample}.cnv"
    output:
        CNV="analysis/preprocessed_files/CNV/{sample}/",
        CNV_bed="analysis/preprocessed_files/CNV/{sample}/{sample}.bed",
    params:
        enhancer=config["enhancer"]
    log:
        "analysis/logs/CNV/{sample}.log"
    shell:
        """
        if [ "{params.enhancer}" == "enhancer" ]; then  (python scripts/cnv_annotate_bed.py -b {input.enhancer} -i {input.CNV} -o {output.CNV}) 2>{log}
        elif [ "{params.enhancer}" == "promoter" ]; then (python scripts/cnv_annotate_bed.py -b {input.promoter} -i {input.CNV} -o {output.CNV}) 2>{log}
        else (python scripts/cnv_annotate_bed.py -b {input.bed} -i {input.CNV} -o {output.CNV}) 2>{log}
        fi
        """


rule merge_cnv:
    input:
        CNV=expand("analysis/preprocessed_files/CNV/{sample}/{sample}.bed", sample=config["runs"]),
    output:
        "analysis/preprocessed_files/CNV/merged_CNV.csv"
    params:
        sample=config["metasheet"]
    shell:
        "python scripts/cnv_merge.py -s {params.sample} -f {input.CNV} > {output}"



rule merge_raw_count:
    input:
        count=expand("analysis/preprocessed_files/read_counts/sample_counts/{sample}.count", sample=config["runs"]),
    output:
        "analysis/preprocessed_files/read_counts/read.counts.raw.csv"
    params:
        sample=config["metasheet"]
    shell:
        "python scripts/read_counts_merge_raw.py -s {params.sample} -f {input.count} > {output}"


rule merge_raw_TPM_count:
    input:
        count=expand("analysis/preprocessed_files/read_counts/sample_counts/{sample}.count", sample=config["runs"]),
        total_count=expand("analysis/preprocessed_files/read_counts/sample_counts/{sample}.total_count",sample=config["runs"])
    output:
        "analysis/preprocessed_files/read_counts/read.counts.raw.TPM.csv"
    params:
        sample=config["metasheet"]
    shell:
        "python scripts/read_counts_merge_raw_TPM.py -s {params.sample} -f {input.count} -t {input.total_count} > {output}"

rule merge_total_count:
    input:
        total_count=expand("analysis/preprocessed_files/read_counts/sample_counts/{sample}.total_count",sample=config["runs"])
    output:
        "analysis/preprocessed_files/read_counts/read.counts.total.csv"
    params:
        sample=config["metasheet"]
    shell:
        "python scripts/read_counts_merge_total.py -s {params.sample} -t {input.total_count} > {output}"


rule merge_count:
    input:
        count=expand("analysis/preprocessed_files/read_counts/sample_counts/{sample}.count", sample=config["runs"]),
        total_count=expand("analysis/preprocessed_files/read_counts/sample_counts/{sample}.total_count",sample=config["runs"])
    output:
        "analysis/preprocessed_files/read_counts/read.counts.rpkm.csv"
    params:
        sample=config["metasheet"]
    shell:
        "python scripts/read_counts_merge.py -s {params.sample} -f {input.count} -t {input.total_count} > {output}"


rule rpkm_threshold:
     input:
        "analysis/preprocessed_files/read_counts/read.counts.rpkm.csv"
     output:
        expand("analysis/clustering_analysis/{param}/read_counts/read.counts.rpkm.threshold.csv",param=params)
     params:
        threshold=config["rpkm_threshold"],
        sample_number=config["mini_num_sample"]
     shell:
        "python scripts/read_counts_rpkm_threshold.py -t {params.threshold} -f {input} -n {params.sample_number} > {output}"

rule scale_count:
     input:
        expand("analysis/clustering_analysis/{param}/read_counts/read.counts.rpkm.threshold.csv",param=params)
     output:
        expand("analysis/clustering_analysis/{param}/read_counts/read.counts.rpkm.threshold.scale.csv",param=params)
     params:
        scale=config["scale"]
     shell:
        "python scripts/read_counts_scale.py -s {params.scale} -f {input} > {output}"

rule filter_count:
     input:
         expand("analysis/clustering_analysis/{param}/read_counts/read.counts.rpkm.threshold.scale.csv",param=params)
     output:
         expand("analysis/clustering_analysis/{param}/read_counts/read.counts.rpkm.threshold.scale.fliter.csv",param=params)
     params:
         filteropt=config["filter-opt"],
         percent=config["filter-percent"]
     shell:
        "python scripts/read_counts_filter.py -o {params.filteropt} -p {params.percent} -f {input} >{output}"

rule pca_plot:
    input:
        countfile=expand("analysis/clustering_analysis/{param}/read_counts/read.counts.rpkm.threshold.scale.fliter.csv",param=params)
    output:
        pca_plot_out=expand("analysis/clustering_analysis/{param}/plots/pca_plot{SSparams}.pdf", param=params,SSparams=SSparams),
        pca_image=expand("analysis/clustering_analysis/{param}/plots/images/pca_plot{SSparams}/", param=params,SSparams=SSparams)
    params:
        SSnumpeaks = config["SSpeaks"],
        annotFile=config['metasheet'],
        #project = config["project"]
    message: "Generating PCA plots"
    shell:
        "Rscript scripts/pca_plot.R {input.countfile} {params.annotFile} "
        "{params.SSnumpeaks} {output.pca_plot_out} {output.pca_image}"

rule tsne_plot:
    input:
        countfile=expand("analysis/clustering_analysis/{param}/read_counts/read.counts.rpkm.threshold.scale.fliter.csv",param=params)
    output:
        tsne_plot_out=expand("analysis/clustering_analysis/{param}/plots/tsne_plot{SSparams}.pdf", param=params,SSparams=SSparams),
        tsne_image=expand("analysis/clustering_analysis/{param}/plots/images/tsne_plot{SSparams}/", param=params,SSparams=SSparams)
    params:
        SSnumpeaks = config["SSpeaks"],
        annotFile=config['metasheet'],
        #project = config["project"]
    message: "Generating tsne plots"
    shell:
        "Rscript scripts/tsne_plot.R {input.countfile} {params.annotFile} "
        "{params.SSnumpeaks} {output.tsne_plot_out} {output.tsne_image}"


rule heatmapSS_plot:
    input:
        countfile=expand("analysis/clustering_analysis/{param}/read_counts/read.counts.rpkm.threshold.scale.fliter.csv",param=params)
    output:
        ss_plot_out=expand("analysis/clustering_analysis/{param}/plots/heatmapSS_plot{SSparams}.pdf",param=params,SSparams=SSparams),
        ss_txt_out=expand("analysis/clustering_analysis/{param}/plots/heatmapSS{SSparams}.txt",param=params,SSparams=SSparams),
        ss_image=expand("analysis/clustering_analysis/{param}/plots/images/heatmapSS_plot{SSparams}/heatmapSS_plot.png",param=params,SSparams=SSparams)
    params:
        SSnumpeaks = config["SSpeaks"],
        annotFile = config["metasheet"],
        project = config["project"],
        cor_method = config["cor_method"],
        dis_method = config["dis_method"]
    message: "Generating Sample-Sample Heatmap"
    shell:
        "Rscript --default-packages=methods,utils scripts/heatmapSS_plot.R {input.countfile} "
        "{params.annotFile} {params.SSnumpeaks} {output.ss_plot_out} {output.ss_txt_out} {params.project} {output.ss_image} {params.cor_method} {params.dis_method} "


rule heatmapSF_plot:
    input:
        countfile=expand("analysis/clustering_analysis/{param}/read_counts/read.counts.rpkm.threshold.scale.fliter.csv",param=params)
    output:
        sf_plot_out=expand("analysis/clustering_analysis/{param}/plots/heatmapSF_plot{SFparams}.pdf",param=params,SFparams=SFparams),
        sf_txt_out=expand("analysis/clustering_analysis/{param}/plots/heatmapSF{SFparams}.txt",param=params,SFparams=SFparams),
        sf_image=expand("analysis/clustering_analysis/{param}/plots/images/heatmapSF_plot{SFparams}/heatmapSF_{num_kmeans_clust}_plot.png",param=params,SFparams=SFparams,num_kmeans_clust=config['num_kmeans_clust']),
    params:
        annotFile=config['metasheet'],
        SFnumpeaks = config["SFpeaks"],
        num_kmeans_clust = config["num_kmeans_clust"],
        project = config["project"],
        cor_method = config["cor_method"],
        dis_method = config["dis_method"]
    message: "Generating Sample-Feature heatmap"
    shell:
        "Rscript --default-packages=methods,utils scripts/heatmapSF_plot.R {input.countfile} "
        "{params.annotFile} {params.SFnumpeaks} {params.num_kmeans_clust} {output.sf_plot_out} {output.sf_txt_out} {params.project} {output.sf_image} {params.cor_method} {params.dis_method} "


rule plots:
    input:
         expand("analysis/clustering_analysis/{param}/read_counts/read.counts.rpkm.threshold.scale.fliter.csv",param=params),
#         expand("analysis/{param}/plots/tsne_plot{SSparams}.pdf", param=params,SSparams=SSparams),
         pca_plot_out=expand("analysis/clustering_analysis/{param}/plots/pca_plot{SSparams}.pdf", param=params,SSparams=SSparams),
         ss_plot_out=expand("analysis/clustering_analysis/{param}/plots/heatmapSS_plot{SSparams}.pdf",param=params,SSparams=SSparams),
         sf_plot_out=expand("analysis/clustering_analysis/{param}/plots/heatmapSF_plot{SFparams}.pdf",param=params,SFparams=SFparams),

if config['CNV_correction'] == 'true':
    include: "./scripts/snakefile.cnv.diffpeak"     # rules specific to sort bam files
else:
    include: "./scripts/snakefile.no.cnv.diffpeak"      # rules specific to copy bam



rule run_limma_and_deseq:
    input:
        deseq_file_list = expand("analysis/differential_peaks/{comparison}/{comparison}.deseq.csv",comparison=config["comparisons"]),
        deseq_with_gene = expand("analysis/differential_peaks/{comparison}/{comparison}.deseq.with.Nearby.Gene.csv",comparison=config["comparisons"]),

# rule GSEA_hallmark:
#     input:
#         deseq_with_gene = "analysis/differential_peaks/{comparison}/{comparison}.deseq.with.Nearby.Gene.csv",
#         ref= "./scripts/h.all.v7.4.entrez.gmt.txt",
#     output:
#         GSEApngplot = "analysis/differential_peaks/{comparison}/{comparison}.GSEA.HALLMARK.png",
#         GSEApdfplot= "analysis/differential_peaks/{comparison}/{comparison}.GSEA.HALLMARK.pdf",
#     shell:
#         "Rscript ./scripts/GSEA_hallmark.R {input.ref} {input.deseq_with_gene} {output.GSEApngplot} {output.GSEApdfplot}"
#
# rule runGSEA_hallmark:
#     input:
#         GSEApngplot = expand("analysis/differential_peaks/{comparison}/{comparison}.GSEA.HALLMARK.png", comparison=config["comparisons"]),
#         GSEApdfplot= expand("analysis/differential_peaks/{comparison}/{comparison}.GSEA.HALLMARK.pdf",comparison=config["comparisons"]),

rule unchanged_peaks:
    input:
        deseq_up = "analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".up.bed",
        deseq_down = "analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_down + ".down.bed",
        raw_count = "analysis/preprocessed_files/read_counts/read.counts.raw.csv"
    output:
        all_peaks = "analysis/differential_peaks/{comparison}/{comparison}.deseq.all.peak.bed",
        deseq_unchanged="analysis/differential_peaks/{comparison}/{comparison}.deseq.unchanged.bed",
    shell:
        "cat {input.raw_count} | tr ',' '\t' | cut -f 1 | tail -n +2 | tr -s '_' '\t' >  {output.all_peaks} ; "
        "bedtools intersect -wa -a {output.all_peaks} -b {input.deseq_up} {input.deseq_down} -v > {output.deseq_unchanged}"


rule deseq_motif:
    input:
        deseq_up = "analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".up.bed",
        deseq_down = "analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_down + ".down.bed",
        genome = expand("ref_files/{ref}/genome.fasta", ref=reference)
    output:
        result_up="analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".up.bed_motif/homerMotifs.all.motifs",
        result_down="analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_down + ".down.bed_motif/homerMotifs.all.motifs",
        path_up="analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".up.bed_motif/",
        path_down="analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_down + ".down.bed_motif/"
    shell:
        "findMotifsGenome.pl {input.deseq_up} {input.genome} {output.path_up} -size given -p 2 -seqlogo -preparsedDir {output.path_up}; "
        "findMotifsGenome.pl {input.deseq_down} {input.genome} {output.path_down} -size given -p 2 -seqlogo -preparsedDir {output.path_down}"

rule deseq_motif_cluster:
    input:
        result_up="analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".up.bed_motif/homerMotifs.all.motifs",
        result_down="analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_down + ".down.bed_motif/homerMotifs.all.motifs",
        path_up="analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".up.bed",
        path_down="analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_down + ".down.bed"
    output:
        result_up="analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".up.bed_motif/knowResults.Motifs.cluster.html",
        result_up_csv="analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".up.bed_motif/knowResults.Motifs.cluster.csv",
        result_down="analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_down + ".down.bed_motif/knowResults.Motifs.cluster.html",
        result_down_csv="analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_down + ".down.bed_motif/knowResults.Motifs.cluster.csv",
    shell:
        "python2.7 scripts/motif_cluster.py -a {input.path_up}_motif/knownResults/ -d 0.7 -o {output.result_up} -c {output.result_up_csv}; "
        "python2.7 scripts/motif_cluster.py -a {input.path_down}_motif/knownResults/ -d 0.7 -o {output.result_down} -c {output.result_down_csv};"

rule getMotifSummary:
    input:
        motif_up_csv="analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".up.bed_motif/knowResults.Motifs.cluster.csv",
        motif_down_csv="analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_down + ".down.bed_motif/knowResults.Motifs.cluster.csv",
    output:
        motif_summary = "analysis/differential_peaks/{comparison}/{comparison}_motif_summary.csv"
    message: "MOTIF: summarizing motif runs"
    shell:
        # files = " -m ".join(input.result_up)
        "Rscript ./scripts/motif_summary.R {input.motif_up_csv} {input.motif_down_csv} {output.motif_summary}"


# rule liftover:
#     input:
#         deseq_up = "analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".up.bed",
#         deseq_down = "analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_down + ".down.bed",
#         lift_chain = expand("ref_files/liftover/{ref}.lift.chain.gz", ref=reference),
#     output:
#         result_up="analysis/differential_peaks/{comparison}/cistrome_toolkit/{comparison}.deseq." + DEseq_up + ".up.lift.bed",
#         result_down="analysis/differential_peaks/{comparison}/cistrome_toolkit/{comparison}.deseq." + DEseq_down + ".down.lift.bed",
#     params:
#         ref = reference
#     run:
#        if params.ref == "hg19" or params.ref == "mm9":
#           shell("liftOver {input.deseq_up} {input.lift_chain} {output.result_up} unmatchedup.bed && rm unmatchedup.bed; liftOver {input.deseq_down} {input.lift_chain} {output.result_down} unmatcheddown.bed && rm -f unmatcheddown.bed; ")
#        else:
#           shell("cp {input.deseq_up}  {output.result_up}; cp {input.deseq_down} {output.result_down}")


# rule cistrome_toolkit:
#     input:
#         result_up="analysis/differential_peaks/{comparison}/cistrome_toolkit/{comparison}.deseq." + DEseq_up + ".up.lift.bed",
#         result_down="analysis/differential_peaks/{comparison}/cistrome_toolkit/{comparison}.deseq." + DEseq_down + ".down.lift.bed",
#         giggle = "ref_files/giggle/CistromeDB.sample.annotation.txt"
#     output:
#         cistrome_up="analysis/differential_peaks/{comparison}/cistrome_toolkit/{comparison}.deseq." + DEseq_up + ".up.cistrome.result.csv",
#         cistrome_down="analysis/differential_peaks/{comparison}/cistrome_toolkit/{comparison}.deseq." + DEseq_down + ".down.cistrome.result.csv",
#     params:
#         giggle_path = "ref_files/giggle",
#         ref = reference
#     shell:
#         "Rscript scripts/cistrom_toolkit.R {input.result_up} {params.giggle_path} {params.ref} {output.cistrome_up} ;"
#         "Rscript scripts/cistrom_toolkit.R {input.result_down} {params.giggle_path} {params.ref} {output.cistrome_down}  "
#
# rule run_cistrome_toolkit:
#     input:
#         cistrome_up=expand("analysis/differential_peaks/{comparison}/cistrome_toolkit/{comparison}.deseq." + DEseq_up + ".up.cistrome.result.csv",comparison=config["comparisons"]),
#         cistrome_down=expand("analysis/differential_peaks/{comparison}/cistrome_toolkit/{comparison}.deseq." + DEseq_down + ".down.cistrome.result.csv",comparison=config["comparisons"]),



rule GSEA:
    input:
        deseq = "analysis/differential_peaks/{comparison}/{comparison}.deseq.with.Nearby.Gene.csv",
    output:
        deseq_rnk = "analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".with.Nearby.Gene.rnk"
    params:
        ref = reference,
        cnvs = config['CNV_correction'],
        Padj = config["Padj"],
        LG2FC = config["LG2FC"],
    shell:
        "bash scripts/gsea_on_deseq.sh -d {input.deseq} -g {params.ref} -v {params.cnvs} -p 1 -c 0 -o {output.deseq_rnk} "

rule run_GSEA:
    input:
        deseq_rnk = expand("analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".with.Nearby.Gene.rnk",comparison=config["comparisons"]),



rule deeptools_diff_peaks:
    input:
        count=expand("analysis/preprocessed_files/bigwig/{sample}.bw", sample=config["runs"]),
        deseq_up = "analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".up.bed",
        deseq_down = "analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_down + ".down.bed",
    output:
        heatmap_deseq = "analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".pdf",
        heatmap_deseq_png = "analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".png",
        heatmap_deseq_path = "analysis/differential_peaks/{comparison}/",
    params:
        s1=lambda wildcards: ".bw analysis/preprocessed_files/bigwig/".join(_getComparison(wildcards.comparison, 1)),
        s2=lambda wildcards: ".bw analysis/preprocessed_files/bigwig/".join(_getComparison(wildcards.comparison, 2)),
        comparsion_chr = "{comparison}"
    message: "deeptools for {wildcards.comparison}"
    run:
       num_lines_up = sum(1 for line in open(input.deseq_up))
       num_lines_down = sum(1 for line in open(input.deseq_down))
       if num_lines_up > 20 and num_lines_down > 20:
         shell("computeMatrix reference-point --referencePoint center -b 2000 -a 2000 -p 4 -R {input.deseq_up} {input.deseq_down} -S analysis/preprocessed_files/bigwig/{params.s1}.bw analysis/preprocessed_files/bigwig/{params.s2}.bw -o {output.heatmap_deseq_path}{wildcards.comparison}_result_matrix.gz --outFileSortedRegions {output.heatmap_deseq_path}{wildcards.comparison}_result.bed && plotHeatmap -m {output.heatmap_deseq_path}{wildcards.comparison}_result_matrix.gz -out {output.heatmap_deseq} --colorMap Reds --refPointLabel \" \" --xAxisLabel \" \" && computeMatrix reference-point --referencePoint center -b 2000 -a 2000 -p 4 -R {input.deseq_up} {input.deseq_down} -S analysis/preprocessed_files/bigwig/{params.s1}.bw analysis/preprocessed_files/bigwig/{params.s2}.bw -o {output.heatmap_deseq_path}{wildcards.comparison}_result_matrix.gz --outFileSortedRegions {output.heatmap_deseq_path}{wildcards.comparison}_result.bed && plotHeatmap -m {output.heatmap_deseq_path}{wildcards.comparison}_result_matrix.gz -out {output.heatmap_deseq_png} --colorMap Reds --refPointLabel \" \" --xAxisLabel \" \" ")
         print("computeMatrix reference-point --referencePoint center -b 2000 -a 2000 -p 4 -R ",input.deseq_up," ",input.deseq_down, " -S analysis/preprocessed_files/bigwig/",params.s1,".bw analysis/preprocessed_files/bigwig/",params.s2,".bw -o ",params.comparsion_chr,"_result_matrix.gz --outFileSortedRegions ",params.comparsion_chr,"_result.bed && plotHeatmap -m ",params.comparsion_chr,"_result_matrix.gz -out ",output.heatmap_deseq," --colorMap Reds --refPointLabel \" \" --xAxisLabel \" \" ",sep='')
       elif num_lines_up < 20 and num_lines_down > 20:
         shell("computeMatrix reference-point --referencePoint center -b 2000 -a 2000 -p 4 -R {input.deseq_down} -S analysis/preprocessed_files/bigwig/{params.s1}.bw analysis/preprocessed_files/bigwig/{params.s2}.bw -o {output.heatmap_deseq_path}{wildcards.comparison}_result_matrix.gz --outFileSortedRegions {output.heatmap_deseq_path}{wildcards.comparison}_result.bed && plotHeatmap -m {output.heatmap_deseq_path}{wildcards.comparison}_result_matrix.gz -out {output.heatmap_deseq} --colorMap Reds --refPointLabel \" \" --xAxisLabel \" \" --regionsLabel \"{input.deseq_down}\" && computeMatrix reference-point --referencePoint center -b 2000 -a 2000 -p 4 -R {input.deseq_down} -S analysis/preprocessed_files/bigwig/{params.s1}.bw analysis/preprocessed_files/bigwig/{params.s2}.bw -o {output.heatmap_deseq_path}{wildcards.comparison}_result_matrix.gz --outFileSortedRegions {output.heatmap_deseq_path}{wildcards.comparison}_result.bed && plotHeatmap -m {output.heatmap_deseq_path}{wildcards.comparison}_result_matrix.gz -out {output.heatmap_deseq_png} --colorMap Reds --refPointLabel \" \" --xAxisLabel \" \" --regionsLabel \"{input.deseq_down}\" ")
         print("computeMatrix reference-point --referencePoint center -b 2000 -a 2000 -p 4 -R ",input.deseq_down, " -S analysis/preprocessed_files/bigwig/",params.s1,".bw analysis/preprocessed_files/bigwig/",params.s2,".bw -o ",params.comparsion_chr,"_result_matrix.gz --outFileSortedRegions ",params.comparsion_chr,"_result.bed && plotHeatmap -m ",params.comparsion_chr,"_result_matrix.gz -out ",output.heatmap_deseq," --colorMap Reds --refPointLabel \" \" --xAxisLabel \" \" ",sep='')
       elif num_lines_up > 20 and num_lines_down < 20:
         shell("computeMatrix reference-point --referencePoint center -b 2000 -a 2000 -p 4 -R {input.deseq_up} -S analysis/preprocessed_files/bigwig/{params.s1}.bw analysis/preprocessed_files/bigwig/{params.s2}.bw -o {output.heatmap_deseq_path}{wildcards.comparison}_result_matrix.gz --outFileSortedRegions {output.heatmap_deseq_path}{wildcards.comparison}_result.bed && plotHeatmap -m {output.heatmap_deseq_path}{wildcards.comparison}_result_matrix.gz -out {output.heatmap_deseq} --colorMap Reds --refPointLabel \" \" --xAxisLabel \" \" --regionsLabel \"{input.deseq_up}\" && computeMatrix reference-point --referencePoint center -b 2000 -a 2000 -p 4 -R {input.deseq_up} -S analysis/preprocessed_files/bigwig/{params.s1}.bw analysis/preprocessed_files/bigwig/{params.s2}.bw -o {output.heatmap_deseq_path}{wildcards.comparison}_result_matrix.gz --outFileSortedRegions {output.heatmap_deseq_path}{wildcards.comparison}_result.bed && plotHeatmap -m {output.heatmap_deseq_path}{wildcards.comparison}_result_matrix.gz -out {output.heatmap_deseq_png} --colorMap Reds --refPointLabel \" \" --xAxisLabel \" \" --regionsLabel \"{input.deseq_up}\" ")
         print("computeMatrix reference-point --referencePoint center -b 2000 -a 2000 -p 4 -R ",input.deseq_up, " -S analysis/preprocessed_files/bigwig/",params.s1,".bw analysis/preprocessed_files/bigwig/",params.s2,".bw -o ",params.comparsion_chr,"_result_matrix.gz --outFileSortedRegions ",params.comparsion_chr,"_result.bed && plotHeatmap -m ",params.comparsion_chr,"_result_matrix.gz -out ",output.heatmap_deseq," --colorMap Reds --refPointLabel \" \" --xAxisLabel \" \" ",sep='')
       else:
          shell("cp scripts/no_enough_diff_peaks.pdf scripts/no_enough_diff_peaks.png {output.heatmap_deseq}")


rule run_deeptools_diff_peaks:
    input:
        heatmap_deseq=expand("analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".pdf",comparison=config["comparisons"]),
        heatmap_deseq_png=expand("analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".png",comparison=config["comparisons"]),



rule deeptools_unchagned:
    input:
        count=expand("analysis/preprocessed_files/bigwig/{sample}.bw", sample=config["bigwig"]),
        deseq_unchanged="analysis/differential_peaks/{comparison}/{comparison}.deseq.unchanged.bed",
    output:
        heatmap_deseq_unchagned = "analysis/differential_peaks/{comparison}/{comparison}.deseq.unchanged.pdf",
        heatmap_deseq_unchagned_png = "analysis/differential_peaks/{comparison}/{comparison}.deseq.unchanged.png",
        heatmap_deseq_unchanged_path = "analysis/differential_peaks/{comparison}/",
    params:
        s1=lambda wildcards: ".bw analysis/preprocessed_files/bigwig/".join(_getComparison(wildcards.comparison, 1)),
        s2=lambda wildcards: ".bw analysis/preprocessed_files/bigwig/".join(_getComparison(wildcards.comparison, 2)),
        comparsion_chr = "{comparison}"
    message: "deeptools for {wildcards.comparison} unchanged peaks"
    run:
         shell("computeMatrix reference-point --referencePoint center -b 2000 -a 2000 -p 4 -R {input.deseq_unchanged} -S analysis/preprocessed_files/bigwig/{params.s1}.bw analysis/preprocessed_files/bigwig/{params.s2}.bw -o {wildcards.comparison}_unchanged_result_matrix.gz --outFileSortedRegions {output.heatmap_deseq_unchanged_path}{wildcards.comparison}_unchanged_result.bed && plotHeatmap -m {output.heatmap_deseq_unchanged_path}{wildcards.comparison}_unchanged_result_matrix.gz -out {output.heatmap_deseq_unchanged_path}{output.heatmap_deseq_unchagned} --colorMap Reds --refPointLabel \" \" --xAxisLabel \" \" --regionsLabel \"{input.deseq_unchanged}\"  && computeMatrix reference-point --referencePoint center -b 2000 -a 2000 -p 4 -R {input.deseq_unchanged} -S analysis/preprocessed_files/bigwig/{params.s1}.bw analysis/preprocessed_files/bigwig/{params.s2}.bw -o {wildcards.comparison}_unchanged_result_matrix.gz --outFileSortedRegions {output.heatmap_deseq_unchanged_path}{wildcards.comparison}_unchanged_result.bed && plotHeatmap -m {output.heatmap_deseq_unchanged_path}{wildcards.comparison}_unchanged_result_matrix.gz -out {output.heatmap_deseq_unchanged_path}{output.heatmap_deseq_unchagned_png} --colorMap Reds --refPointLabel \" \" --xAxisLabel \" \" --regionsLabel \"{input.deseq_unchanged}\"  ")
         print("computeMatrix reference-point --referencePoint center -b 2000 -a 2000 -p 4 -R ",input.deseq_unchanged, " -S analysis/preprocessed_files/bigwig/",params.s1,".bw analysis/preprocessed_files/bigwig/",params.s2,".bw -o ",params.comparsion_chr,"_unchanged_result_matrix.gz --outFileSortedRegions ",params.comparsion_chr,"_unchanged_result.bed && plotHeatmap -m ",params.comparsion_chr,"_unchanged_result_matrix.gz -out ",output.heatmap_deseq_unchagned," --colorMap Reds --refPointLabel \" \" --xAxisLabel \" \" --regionsLabel ",input.deseq_unchanged, sep='')



rule nearby_gene:
    input:
        deseq_up = "analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".up.bed",
        deseq_down = "analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_down + ".down.bed",
        refseqGenes = expand("ref_files/{ref}/refGene.{ref}.id.bed",ref=reference)
    output:
        deseq_up_gene = "analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".up.Nearby.Gene.xls",
        deseq_down_gene = "analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_down + ".down.Nearby.Gene.xls",
    shell:
        "sort -k1,1 -k2,2n {input.deseq_up} > {input.deseq_up}.sort && bedtools closest -a {input.deseq_up}.sort -b {input.refseqGenes} -t first > {output.deseq_up_gene} ; "
        "sort -k1,1 -k2,2n {input.deseq_down} > {input.deseq_down}.sort && bedtools closest -a {input.deseq_down}.sort -b {input.refseqGenes} -t first > {output.deseq_down_gene} "

rule de_summary_plot:
    input:
        deseq_file_list=expand("analysis/differential_peaks/{comparison}/{comparison}.deseq.csv",comparison=config["comparisons"]),
    output:
        #csv="analysis/differential_peaks/de_summary.csv",
        pdf="analysis/differential_peaks/de_summary.pdf",
        png="analysis/differential_peaks/de_summary.png",
    message: "Creating Differential Expression summary"
    params:
        csv="analysis/differential_peaks/de_summary.csv",
    run:
        deseq_file_string = ' -f '.join(input.deseq_file_list)
        shell("perl scripts/get_de_summary_table.pl -f {deseq_file_string} 1>{params.csv}")
        shell("Rscript scripts/de_summary.R {params.csv} {output.pdf} {output.png}")
#        shell("chmod -R 777 analysis")


rule png_to_folder:
    input:
        pca_plot=expand("analysis/clustering_analysis/{param}/plots/pca_plot{SSparams}.pdf", param=params,SSparams=SSparams),
        ss_plot=expand("analysis/clustering_analysis/{param}/plots/heatmapSS_plot{SSparams}.pdf",param=params,SSparams=SSparams),
        sf_plot=expand("analysis/clustering_analysis/{param}/plots/heatmapSF_plot{SFparams}.pdf",param=params,SFparams=SFparams),
        heatmap_deseq_plot=expand("analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".pdf",comparison=config["comparisons"]),
        pca_image=expand("analysis/clustering_analysis/{param}/plots/images/pca_plot{SSparams}/", param=params,SSparams=SSparams),
        ss_image=expand("analysis/clustering_analysis/{param}/plots/images/heatmapSS_plot{SSparams}/heatmapSS_plot.png",param=params,SSparams=SSparams),
        sf_image=expand("analysis/clustering_analysis/{param}/plots/images/heatmapSF_plot{SFparams}/heatmapSF_{num_kmeans_clust}_plot.png",param=params,SFparams=SFparams,num_kmeans_clust=config['num_kmeans_clust']),
        # GSEApngplot= expand("analysis/differential_peaks/{comparison}/{comparison}.GSEA.HALLMARK.png",comparison=config["comparisons"]),
        # GSEApdfplot=expand("analysis/differential_peaks/{comparison}/{comparison}.GSEA.HALLMARK.pdf",comparison=config["comparisons"]),
        de_image="analysis/differential_peaks/de_summary.png",
        de_plot="analysis/differential_peaks/de_summary.pdf",
        heatmap_deseq=expand("analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".png",comparison=config["comparisons"]),
    output:
        report_folder="analysis/report/images/",
    message: "Moving png to the report folder..."
    shell:
        "cp -r {input.pca_plot} {input.ss_plot} {input.sf_plot} {input.heatmap_deseq_plot} {input.pca_image} {input.ss_image} {input.sf_image} {input.de_image} {input.de_plot} {input.heatmap_deseq} {output.report_folder}"

from scripts.cobra_report import get_sphinx_report
from snakemake.utils import report
_branding=["Center for Functional Cancer Epigenetics, DFCI", "./scripts/cfce.jpg"]


rule all:
    input:
        expand("analysis/clustering_analysis/{param}/read_counts/read.counts.rpkm.threshold.scale.fliter.csv",param=params),
        pca_plot_out=expand("analysis/clustering_analysis/{param}/plots/pca_plot{SSparams}.pdf", param=params,SSparams=SSparams),
        ss_plot_out=expand("analysis/clustering_analysis/{param}/plots/heatmapSS_plot{SSparams}.pdf",param=params,SSparams=SSparams),
        sf_plot_out=expand("analysis/clustering_analysis/{param}/plots/heatmapSF_plot{SFparams}.pdf",param=params,SFparams=SFparams),
        deseq_file_list=expand("analysis/differential_peaks/{comparison}/{comparison}.deseq.csv",comparison=config["comparisons"]),
        deseq_with_gene = expand("analysis/differential_peaks/{comparison}/{comparison}.deseq.with.Nearby.Gene.csv",comparison=config["comparisons"]),
        heatmap_deseq=expand("analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".pdf",comparison=config["comparisons"]),
        deseq_unchanged=expand("analysis/differential_peaks/{comparison}/{comparison}.deseq.unchanged.bed",comparison=config["comparisons"]),
        heatmap_deseq_unchagned = expand("analysis/differential_peaks/{comparison}/{comparison}.deseq.unchanged.pdf",comparison=config["comparisons"]) if config['unchanged_heatmap'] == 'true' else expand("analysis/clustering_analysis/{param}/read_counts/read.counts.rpkm.threshold.scale.fliter.csv",param=params) ,
        # GSEApngplot= expand("analysis/differential_peaks/{comparison}/{comparison}.GSEA.HALLMARK.png",comparison=config["comparisons"]),
        # GSEApdfplot=expand("analysis/differential_peaks/{comparison}/{comparison}.GSEA.HALLMARK.pdf",comparison=config["comparisons"]),
        force_run_upon_config_change = "config.yaml",
#        deseq_rnk = expand("analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".with.Nearby.Gene.rnk",comparison=config["comparisons"]),
#         cistrome_up=expand("analysis/differential_peaks/{comparison}/cistrome_toolkit/{comparison}.deseq." + DEseq_up + ".up.cistrome.result.csv",comparison=config["comparisons"]),
#         cistrome_down=expand("analysis/differential_peaks/{comparison}/cistrome_toolkit/{comparison}.deseq." + DEseq_down + ".down.cistrome.result.csv",comparison=config["comparisons"]),
        result_up=expand("analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".up.bed_motif/homerMotifs.all.motifs",comparison=config["comparisons"]) if config['motif'] == 'true' else expand("analysis/clustering_analysis/{param}/read_counts/read.counts.rpkm.threshold.scale.fliter.csv",param=params) ,
        result_down=expand("analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_down + ".down.bed_motif/homerMotifs.all.motifs",comparison=config["comparisons"]) if config['motif'] == 'true' else expand("analysis/clustering_analysis/{param}/read_counts/read.counts.rpkm.threshold.scale.fliter.csv",param=params) ,
        result_up_cluster=expand("analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".up.bed_motif/knowResults.Motifs.cluster.html",comparison=config["comparisons"]) if config['motif'] == 'true' else expand("analysis/clustering_analysis/{param}/read_counts/read.counts.rpkm.threshold.scale.fliter.csv",param=params) ,
        result_down_cluster=expand("analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_down + ".down.bed_motif/knowResults.Motifs.cluster.html",comparison=config["comparisons"]) if config['motif'] == 'true' else expand("analysis/clustering_analysis/{param}/read_counts/read.counts.rpkm.threshold.scale.fliter.csv",param=params) ,
        result_up_gene=expand("analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_up + ".up.Nearby.Gene.xls",comparison=config["comparisons"]),
        result_down_gene=expand("analysis/differential_peaks/{comparison}/{comparison}.deseq." + DEseq_down + ".down.Nearby.Gene.xls",comparison=config["comparisons"]),
        motif_summary=expand("analysis/differential_peaks/{comparison}/{comparison}_motif_summary.csv",comparison=config["comparisons"]) if config['motif'] == 'true' else expand("analysis/clustering_analysis/{param}/read_counts/read.counts.rpkm.threshold.scale.fliter.csv",param=params) ,
        mapping_csv="analysis/preprocessed_files/align/mapping.csv",
        peaks_stat="analysis/preprocessed_files/peaks/peakStats.csv",
        seqsum="analysis/report/sequenceSummary.csv",
        force_run_upon_meta_change = config['metasheet'],
        de_plot="analysis/differential_peaks/de_summary.pdf",
        report_folder="analysis/report/images/",
    output:
        html="analysis/report/report.html"
    message: "Generating COBRA report"
    run:
        sphinx_str = get_sphinx_report(config)
        report(sphinx_str, output[0], metadata=_branding[0], **{'Copyrights:':_branding[1]})
#        shell("chmod -R 777 analysis")



rule extract_cluster:
     input:
         sf_txt_out=expand("analysis/clustering_analysis/{param}/plots/heatmapSF{SFparams}.txt",param=params,SFparams=SFparams)
     output:
         path=expand("analysis/clustering_analysis/{param}/cluster/",param=params,SFparams=SFparams),
         cluster=expand("analysis/clustering_analysis/{param}/cluster/{bed}.bed",param=params,SFparams=SFparams,bed=list(range(1,int(config["num_kmeans_clust"])+1)))
     shell:
         "mkdir -p {output.path} && bash scripts/cut_cluster.sh -d {input.sf_txt_out} -o {output.path} "


rule sort_bed:
    input:
        lambda wildcards: "analysis/clustering_analysis/"+str([wildcards.param][0])+"/cluster/"+str(wildcards.bed)+".bed"
    output:
        "analysis/clustering_analysis/{param}/cluster/{bed}_sort.bed",
    shell:
        "sort -k1,1 -k2,2n {input} > {output}"


rule peak_gene:
    input:
        bed = "analysis/clustering_analysis/{param}/cluster/{bed}_sort.bed",
        refseqGenes = expand("ref_files/{ref}/refGene.{ref}.id.bed",ref=reference)
    output:
        peak_gene="analysis/clustering_analysis/{param}/cluster/{bed}_peak_genes.xls",
    shell:
        "bedtools closest -a {input.bed} -b {input.refseqGenes} > {output}"

rule motif_homer:
    input:
        bed = lambda wildcards: "analysis/clustering_analysis/"+str([wildcards.param][0])+"/cluster/"+str(wildcards.bed)+".bed",
        genome = expand("ref_files/{ref}/genome.fasta", ref=reference)
    output:
        result="analysis/clustering_analysis/{param}/motif/cluster_{bed}/knownResults.html",
        path="analysis/clustering_analysis/{param}/motif/cluster_{bed}/"
    shell:
        "findMotifsGenome.pl {input.bed} {input.genome} {output.path} -size given ; "

rule liftover_cluster:
    input:
        bed = "analysis/clustering_analysis/{param}/cluster/{bed}_sort.bed",
        lift_chain = expand("ref_files/liftover/{ref}.lift.chain.gz", ref=reference),
    output:
        bed = "analysis/clustering_analysis/{param}/cistrome_toolkit/{bed}.lift.bed",
    params:
        ref = reference
    run:
       if params.ref == "hg19" or params.ref == "mm9":
          shell("liftOver {input.bed} {input.lift_chain} {output.bed} unmatchedup.bed && rm -f unmatchedup.bed")
       else:
          shell("cp {input.bed}  {output.bed}")

rule cistrome_toolkit_cluster:
    input:
        bed = "analysis/clustering_analysis/{param}/cistrome_toolkit/{bed}.lift.bed",
        giggle = "ref_files/giggle/CistromeDB.sample.annotation.txt"
    output:
        cistrome = "analysis/clustering_analysis/{param}/cistrome_toolkit/cluster_{bed}.cistrome.result.csv",
    params:
        giggle_path = "ref_files/giggle",
        ref = reference
    shell:
        "Rscript scripts/cistrom_toolkit.R {input.bed} {params.giggle_path} {params.ref} {output.cistrome} ;"


rule cluster_analysis:
    input:
        expand("analysis/clustering_analysis/{param}/cluster/{bed}_peak_genes.xls",param=params,SFparams=SFparams,bed=list(range(1,int(config["num_kmeans_clust"])+1))),
        expand("analysis/clustering_analysis/{param}/motif/cluster_{bed}/knownResults.html",param=params,SFparams=SFparams,bed=list(range(1,int(config["num_kmeans_clust"])+1))),
        expand("analysis/clustering_analysis/{param}/cistrome_toolkit/cluster_{bed}.cistrome.result.csv",param=params,SFparams=SFparams,bed=list(range(1,int(config["num_kmeans_clust"])+1))),

