
# CoBRA - Containerized Bioinformatics workflow for Reproducible ChIP/ATAC-seq Analysis

# Summary:

__CoBRA__ is a comprehensive ChIP/ATAC‐seq analysis tool built using [snakemake](https://bitbucket.org/snakemake/snakemake/wiki/Home)  and [Docker](https://docs.docker.com/) which allows for escalable, reproducible, portable and easy-to-use workflows. 

CoBRA combines the use of several dozen ChIP/ATAC‐seq tools, suites, and packages to create a complete pipeline that takes ChIP/ATAC‐seq analysis from unsupervised analyses, differential peak calling, and downstream pathway analysis. In addition, CoBRA has been outfitted with several recently published tools that allow for better normalziation and CNV correction. The results are compiled in a simple and highly visual report containing the key figures to explain the analysis, and then compiles all of the relevant files, tables, and pictures into an easy to navigate folder.

# Code
https://bitbucket.org/cfce/cobra

# Documentation
https://cfce-cobra.readthedocs.io/en/latest/

# Quick Start and Installation
https://cfce-cobra.readthedocs.io/en/latest/install.html

# Citation

Xintao Qiu#, Avery S. Feit#, Ariel Feiglin#, Yingtian Xie, Nikolas Kesten, Len Taing, Joseph Perkins, Shengqing Gu, Yihao Li, Paloma Cejas, Ningxuan Zhou, Rinath Jeselsohn, Myles Brown, X. Shirley Liu, Henry W. Long. CoBRA: Containerized Bioinformatics Workflow for Reproducible ChIP/ATAC-seq Analysis. 2021. Genomics, Proteomics & Bioinformatics.

Open Access. DOI: https://doi.org/10.1016/j.gpb.2020.11.007

# License
GNU General Public License v2.0


