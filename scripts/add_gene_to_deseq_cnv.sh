#!/usr/bin/bash

usage() { echo "Usage: $0 [-d <deseqfile> -r <reference> -o <outputfile>]"; exit 1; }

[ $# -eq 0 ] && usage

name=
ref=
out=

while getopts :d:r:o: OPTION
do
    case $OPTION in
        d)
            name="$OPTARG"
            ;;
        r)
            ref="$OPTARG"
            ;;
        o)
            out="$OPTARG"
            ;;

        \?)                       
            usage
            ;;
    esac
done

outputfile=$out
deseqfile=$name
#prefix=$(echo ${deseqfile} | cut -d '.' -f 1) 

cat $deseqfile | tr ',' '\t' | awk -v OFS='\t' '{print $1}' | tr -s '_' '\t' | awk '{print $1"\t"$2"\t"$3"\t"$1"_"$2"_"$3}'| tail -n +2  > ${deseqfile}.deseq.bed
 
sort -k1,1 -k2,2n ${deseqfile}.deseq.bed > ${deseqfile}.deseq.bed.sort && bedtools closest -a ${deseqfile}.deseq.bed.sort -b $ref -t first > ${deseqfile}.deseq.bed.Nearby.Gene.new.xls
 
cut -f 1,2,3,4,8,9 ${deseqfile}.deseq.bed.Nearby.Gene.new.xls > ${deseqfile}.deseq.bed.Nearby.Gene.list
 
cat ${deseqfile}.deseq.bed.Nearby.Gene.list | sort -k4 > ${deseqfile}.deseq.bed.Nearby.Gene.order.list
 
cat $deseqfile | tr ',' '\t' | tail -n +2   | sort -k1 > ${deseqfile}.deseq.order.list

paste ${deseqfile}.deseq.order.list ${deseqfile}.deseq.bed.Nearby.Gene.order.list  > ${deseqfile}.deseq.final.tsv

cut -f 1,2,3,4,5,6,7,8,9,10,11,16,17 ${deseqfile}.deseq.final.tsv | tr '\t' ',' > ${deseqfile}.deseq.final2.tsv

sort -k7 -n -t, ${deseqfile}.deseq.final2.tsv > ${deseqfile}.deseq.final3.tsv

awk 'BEGIN{FS=OFS=","} {for (i=1;i<=2;i++) gsub(/_/," ",$i)} 1' ${deseqfile}.deseq.final3.tsv > ${deseqfile}.deseq.final4.tsv

echo 'id,baseMean,log2FoldChange,lfcSE,stat,pvalue,padj,ctrl_mean,treat_mean,ctrl_cnv,treat_cnv,gene_id,gene_name' | cat - ${deseqfile}.deseq.final4.tsv > $outputfile

rm ${deseqfile}.deseq.bed
rm ${deseqfile}.deseq.bed.sort
rm ${deseqfile}.deseq.bed.Nearby.Gene.new.xls
rm ${deseqfile}.deseq.bed.Nearby.Gene.list
rm ${deseqfile}.deseq.bed.Nearby.Gene.order.list
rm ${deseqfile}.deseq.order.list
rm ${deseqfile}.deseq.final.tsv
rm ${deseqfile}.deseq.final2.tsv
rm ${deseqfile}.deseq.final3.tsv
rm ${deseqfile}.deseq.final4.tsv
