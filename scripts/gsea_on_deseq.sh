#!/bin/bash

echo -e "###GSEA start: " `date +%y-%m-%d.%H:%M:%S` "\n"

usage() { echo "Usage: $0 [-d <deseq_data>] [-g <genome>] [-v <cnv>] [-p <Padj>] [-c <LG2FC>] [-o <output>]  "; exit 1; }


while getopts :d:g:v:p:c:o: OPTION
do
    case $OPTION in
        d)
            input="$OPTARG"
            ;;
        g)
	    genome="$OPTARG"
	    ;;
	v)
	    cnv="$OPTARG"
            ;;			
        p)
            padj="$OPTARG"
            ;;
        c)
            fc="$OPTARG"
            ;;
        o)
            data="$OPTARG"
            ;;
        \?)
            usage
            ;;
    esac
done

echo $input, $genome, $cnv , $padj, $fc , $data

if [ $cnv == "true" ];then
	cat $input | awk -F, -v padj=$padj -v fc=$fc  '{if (($7 <= padj && $3 >= fc) || ($7 <= padj && $3 <= -fc))  print $13"\t"$3}' | tr ',' '\t' > $data
else
	cat $input | awk -F, -v padj=$padj -v fc=$fc  '{if (($7 <= padj && $3 >= fc) || ($7 <= padj && $3 <= -fc))  print $11"\t"$3}' | tr ',' '\t' > $data
fi


if [ ${genome:0:2} == "mm" ];then
	coll="-collapse true -chip gseaftp.broadinstitute.org://pub/gsea/annotations/ENSEMBL_mouse_gene.chip"
else
	coll=""
fi

java -Xmx1024m -cp scripts/gsea-3.0.jar xtools.gsea.GseaPreranked -gmx ftp.broadinstitute.org://pub/gsea/gene_sets/h.all.v2022.1.Hs.symbols.gmt -norm meandiv -nperm 1000 -rnk $data -scoring_scheme weighted -rpt_label "H_Hallmark" -create_svgs false -make_sets true -plot_top_x 30 -set_max 500 -set_min 15 -zip_report false -out ${data%/*}"/GSEA" -gui false $coll

java -Xmx2048m -cp scripts/gsea-3.0.jar xtools.gsea.GseaPreranked -gmx ftp.broadinstitute.org://pub/gsea/gene_sets/h.all.v2022.1.Hs.symbols.gmt -norm meandiv -nperm 1000 -rnk $data -scoring_scheme weighted -rpt_label "C2_Curated" -create_svgs false -make_sets true -plot_top_x 30 -set_max 500 -set_min 15 -zip_report false -out ${data%/*}"/GSEA" -gui false $coll
