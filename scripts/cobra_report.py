#!/usr/bin/env python

# vim: syntax=python tabstop=4 expandtab

#-------------------------------
# @author: Mahesh Vangala
# @email: vangalamaheshh@gmail.com
# @date: Aug, 1, 2015
#-------------------------------

import os
import glob
import subprocess

import pandas as pd
from scripts.csv_to_sphinx_table import get_sphinx_table
from snakemake.report import data_uri
from tabulate import tabulate


def get_sphinx_report(config):
    comps = config["comparisons"]
    SSparams = "_" + str(config["filter-percent"]) + "_percent"
    DEseq_up = "Padj" + str(config["Padj"]) + ".LG2FC." + str(config["LG2FC"])

    git_commit_string = "XXXXXX"
    git_link = 'https://cfce-cobra.readthedocs.io/en/latest/'
    #Check for .git directory
    # if os.path.exists("viper/.git"):
    #     git_commit_string = subprocess.check_output('git --git-dir="viper/.git" rev-parse --short HEAD',shell=True).decode('utf-8').strip()
    #     git_link = 'https://bitbucket.org/cfce/viper/commits/' + git_commit_string
    file_dict = {
        'cfce_logo': "scripts/CFCE_Logo_Final.jpg",
        'heatmapSF_plot': "analysis/report/images/heatmapSF_" + str(config['num_kmeans_clust']) + "_plot.png",
        'heatmapSS_plot': "analysis/report/images/heatmapSS_plot.png",
        'DEsummary_plot': "analysis/report/images/de_summary.png"
    }
    copy_file_dict = {}
    for key in file_dict.keys():
        copy_file_dict[key] = file_dict[key]
    for file_token in file_dict.keys():
        if not os.path.isfile(file_dict[file_token]):
            del copy_file_dict[file_token]
        else:
            copy_file_dict[file_token] = data_uri(copy_file_dict[file_token])
    file_dict = copy_file_dict
    pca_png_list = []
    deseq_heatmap_list = []
    # SF_png_list = []
    peaks_out = "analysis/differential_peaks/de_summary.csv"
    reads_csv = "analysis/report/sequenceSummary.csv"
    motif_csv_list = []
    # gsea_list = []

    f = pd.read_csv(peaks_out, index_col=0)
    hdr = ['Comparison', 'padj<1&lg2FC>2', 'padj<1&lg2FC<-2', 'padj<0.5&lg2FC>2', 'padj<0.5&lg2FC<-2', 'padj<1&lg2FC>1',
           'padj<1&lg2FC<-1', 'padj<0.5&lg2FC>1', 'padj<0.5&lg2FC<-1']
    peaks_out = tabulate(f, hdr, tablefmt='rst')

    for pca_plot in sorted(glob.glob("./analysis/report/images/pca_plot" + str(SSparams) + "/pca_plot*.png")):
        if "pca_plot_scree.png" not in pca_plot:
            pca_png_list.append(data_uri(pca_plot))

    if(os.path.isfile("./analysis/report/images/pca_plot" + str(SSparams) + "/pca_plot_scree.png")):
        pca_png_list.append(data_uri("./analysis/report/images/pca_plot" + str(SSparams) + "/pca_plot_scree.png"))

    for comp in comps:
        tmp_f = "./analysis/report/images/%s.deseq.%s.png" % (comp, DEseq_up)
        if os.path.isfile(tmp_f):
            deseq_heatmap_list.append(data_uri(tmp_f))
        tmp_f2 = "./analysis/differential_peaks/%s/%s_motif_summary.csv" % (comp, comp)
        if os.path.isfile(tmp_f2):
            motifs = {}
            f=open(tmp_f2)
            for l in f:
                tmp = l.strip().split(',')
                motifs[tmp[0]] = {'direction': tmp[1], 'logo': tmp[2], 'motifName': tmp[3], 'PValue': tmp[4],
                                  'log(PValue)': tmp[5], '% of Targets Sequences with Motif': tmp[6],
                                  '% of Background Sequences with Motif': tmp[7]}
            f.close()

            hdr = ['direction', 'logo', 'motifName', 'PValue', 'log(PValue)', '% of Targets Sequences with Motif',
                   '% of Background Sequences with Motif']
            rest = []
            for i in motifs:
                motif_logo = ".. image:: %s" % data_uri(motifs[i]['logo'])
                rest.append([motifs[i]['direction'], motif_logo, motifs[i]['motifName'], motifs[i]['PValue'],
                             motifs[i]['log(PValue)'], motifs[i]['% of Targets Sequences with Motif'],
                             motifs[i]['% of Background Sequences with Motif']])
            ret = tabulate(rest, hdr, tablefmt='rst')
            motif_csv_list.append(ret)

        # tmp_f3 = "./analysis/report/images/%s.GSEA.HALLMARK.png" % comp
        # if os.path.isfile(tmp_f3):
        #     gsea_list.append(data_uri(tmp_f3))

    if pca_png_list:
        file_dict['pca_png_list'] = pca_png_list
    if deseq_heatmap_list:
        file_dict['deseq_heatmap_list'] = deseq_heatmap_list
    if motif_csv_list:
        file_dict['motif_csv_list'] = motif_csv_list
    # if gsea_list:
    #     file_dict['gsea_list'] = gsea_list
    # if SF_png_list:
    #     file_dict['sf_png_list'] = SF_png_list
    # if gsea_list:
    #     file_dict['gsea_png_list'] = gsea_list
    report = """

.. 
"""
    report += "\n\t.. image:: " + file_dict['cfce_logo']
    report += """

=====================================================================================
COBRA: Containerized Bioinformatics workflow for Reproducible ChIP/ATAC-seq Analysis 
=====================================================================================

Overall Reads
^^^^^^^^^^^^^

"""
    report += "\n" + get_sphinx_table(reads_csv) + "\n"

    report += "\n"
    report += """
    

Principle Component Analysis
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    High dimensional expression data are mathmatically reduced to principle
    components that can be used to describe variation across samples in fewer dimensions to allow human interpretation.
    Principle component 1 \(PC1\) accounts for the most amount of variation across samples, PC2 the second most, and so on. These PC1 vs PC2 plots
    are colored by sample annotation to demontrate how samples cluster together \(or not\) in reduced dimensional space.
    For more detailed description of Princilple Component Analysis, start with `wikipedia`_.

    .. _wikipedia: https://en.wikipedia.org/wiki/Principal_component_analysis

"""

    if 'pca_png_list' in file_dict:
        if len(file_dict['pca_png_list']) > 1:
            report += "\n\t.. image:: " + "\n\t.. image:: ".join(file_dict['pca_png_list'][:-1]) + "\n"
            report += "\n\t" + 'This plot indicates how much of the overall variance is described by the principle components in descending order.' + "\n\n\t.. image:: " + file_dict['pca_png_list'][-1] + "\n"
        else:
            report += "\n\t.. image:: " + "\n\t.. image:: ".join(file_dict['pca_png_list'][0]) + "\n"

    report += "\n"
    report += """
Sample-to-Sample Correlation Heatmap
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    This heatmap displays hierarchical clustering of spearman rank correlations across samples.
    Highly correlated samples will cluster together and be represented in red. Samples that do not correlate will be represented in blue.

"""

    if 'heatmapSS_plot' in file_dict:
        report += "\n\n\t.. image:: " + file_dict['heatmapSS_plot'] + "\n\n\t" + 'Sample-to-Sample data matrix is /analysis/plots/heatmapSS.txt' + "\n"

    report += "\n"
    report += """
Sample-Feature Correlation Heatmap
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    This heatmap illustrates hierarchical clustering of samples based on the top 5 percent or roughly 1000 most variable genes or "features."
    The top colomn color annotations are presented to help identify how sample types, groups, or treatments are clustering together \(or not\).

"""

    if 'heatmapSF_plot' in file_dict:
        report += "\n\n\t.. image:: " + file_dict['heatmapSF_plot'] + "\n";

    report += "\n"
    report += """   
    
   
Differential peaks
=============================
    Differential peak analysis was performed using `DESeq2`_.\n
    Full analysis output tables are are available in /analysis/differential_peaks

    .. _DESeq2: http://www.ncbi.nlm.nih.gov/pmc/articles/PMC4302049/

    This summary image shows the number of peaks that are up regulated and down regulated across each comparison at different adjusted P-value cut-offs.

"""

    if 'DEsummary_plot' in file_dict:
        report += "\n\n\t.. image:: " + file_dict['DEsummary_plot'] + "\n\t\t:height: 500" + "\n"
        report += "\n" + peaks_out + "\n\n\n"
    report += "\n"
    report += """ 
    
Differential heatmaps and motifs
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

"""
    if 'DEsummary_plot' in file_dict:
        if 'deseq_heatmap_list' in file_dict:
            for i in range(0, len(file_dict['deseq_heatmap_list'])):
                report += "\n\t **" + str(comps[i]) + "** \n"
                report += "\n\n\t.. image:: " + file_dict['deseq_heatmap_list'][i] + "\n\t\t:height: 500" + "\n\t\t:align: center" + "\n\n\n"
                # report += "\n\n\t.. image:: " + file_dict['gsea_list'][i] + "\n\t\t:height: 500" + "\n\t\t:align: center" + "\n"
                if 'motif_csv_list' in file_dict:
                    report += "\n" + motif_csv_list[i] + "\n\n\n"
                report += "================================================================\n"

    report += "\n"
    report += """
    
"""
        # report += "\n\n\t.. image:: " + data_uri(cdr_cpk_plot) +"\n"

    # report += "\n\n**This report is generated using VIPER version** [ `" + git_commit_string + "`_ ].\n"
    # report += "\t.. _" + git_commit_string + ': ' + git_link + "\n\n"
    report += "\n\n**To cite COBRA:\nXintao Qiu, Avery S. Feit, Ariel Feiglin, Yingtian Xie, Nikolas Kesten, Len Taing, Joseph Perkins, Shengqing Gu, Yihao Li, Paloma Cejas, Ningxuan Zhou, Rinath Jeselsohn, Myles Brown, X. Shirley Liu, Henry W. Long. CoBRA: Containerized Bioinformatics Workflow for Reproducible ChIP/ATAC-seq Analysis. 2021. Genomics, Proteomics & Bioinformatics.**\n\n"
    return report + "\n"


