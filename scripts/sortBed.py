#!/usr/bin/env python
"""Given a bed file, sort it by the score--given in the 5th col
"""
import sys
from optparse import OptionParser

def readBed(bed_file, asc=False):
    """reads a bed file and returns a sorted list of tuples
    """
    _order = ['chr10','chr11','chr12','chr13','chr14','chr15','chr16','chr17',
              'chr18','chr19','chr1','chr20','chr21','chr22','chr2',
              'chr3','chr4','chr5','chr6','chr7','chr8','chr9','chrM','chrX', 'chrY'] 
    f = open(bed_file)
    ret = []
    for l in f:
        tmp = l.strip().split("\t")
        #only include peaks that are in _order
        if tmp[0] in _order:
            ret.append(tuple(tmp))
    f.close()
    #return sorted(ret, key=lambda x: float(x[4]), reverse=not asc)
    return sorted(ret, key=lambda x: _order.index(x[0]), reverse=not asc)

def main():
    optparser = OptionParser()
    optparser.add_option("-f", "--file", help="bed file to sort")
    optparser.add_option("-a", "--asc", default=False, action="store_true",
                         help="sort by ascending score, default: False i.e. descending score")
    (options, args) = optparser.parse_args(sys.argv)

    if not options.file:
        print "USAGE: sortBed.py -f [bed file] -a (optional: sort by ascending--default: sort by descending score)"
        sys.exit(-1)
    
    tmp = readBed(options.file, options.asc)

    for r in tmp:
        print "\t".join(r)
    
if __name__=='__main__':
    main()
