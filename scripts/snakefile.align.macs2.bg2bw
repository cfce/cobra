#!/usr/bin/env python

import os
import sys
import subprocess
import pandas as pd
import yaml

from string import Template

def getRuns(config):
    """parse metasheet for Run groupings"""
    ret = {}

    #LEN: Weird, but using pandas to handle the comments in the file
    #KEY: need skipinitialspace to make it fault tolerant to spaces!
    metadata = pd.read_table(config['metasheet'], index_col=0, sep=',', comment='#', skipinitialspace=True)
    f = metadata.to_csv().split() #make it resemble an actual file with lines
    #SKIP the hdr
    for l in f[1:]:
        tmp = l.strip().split(",")
        #print(tmp)
        ret[tmp[0]] = tmp[1:]

    #print(ret)
    config['runs'] = ret
    return config

def addPy2Paths_Config(config):
    """ADDS the python2 paths to config"""
    conda_root = subprocess.check_output('conda info --root',shell=True).decode('utf-8').strip()
    conda_path = os.path.join(conda_root, 'pkgs')
    config["python2_pythonpath"] = os.path.join('/usr/bin/python2.7')
    
    if not "python2" in config or not config["python2"]:
        config["python2"] = os.path.join('/usr/bin/python2.7')


    if not "macs2_path" in config or not config["macs2_path"]:
        config["macs2_path"] = os.path.join(conda_root, 'envs', 'cobra', 'bin', 'macs2')

def loadRef(config):
    """Adds the static reference paths found in config['ref']
    NOTE: if the elm is already defined, then we DO NOT clobber the value
    """
    f = open(config['ref'])
    ref_info = yaml.safe_load(f)
    f.close()
    #print(ref_info[config['assembly']])
    for (k,v) in ref_info[config['assembly']].items():
        #NO CLOBBERING what is user-defined!
        if k not in config:
            config[k] = v

#---------  CONFIG set up  ---------------
configfile: "config.yaml"   # This makes snakemake load up yaml into config 
config = getRuns(config)
addPy2Paths_Config(config)

#NOW load ref.yaml - SIDE-EFFECT: loadRef CHANGES config
loadRef(config)
#-----------------------------------------


_align_min_threads=8


#MODULE: Align fastq files to genome - BWA specific calls

def getFastq(wildcards):
    return config["fastq"][wildcards.sample]

def getBam(wildcards):
    """This input fn will check to see if the user specified a .fastq or a .bam
    for the sample.  IF the former (.fastq), will simply return the canonical
    path, otherwise (.bam) will return the user-specified (bam) path"""
    #CHECK first entry's file suffix
    s = wildcards.sample
    first_file = config["fastq"][wildcards.sample][0]
    ret = "analysis/preprocessed_files/align/%s/%s.bam" % (s,s)
    if first_file.endswith('.bam'):
        #CLEANER to check for .bam vs (.fastq, fq, fq.gz, etc)
        ret = first_file
    return [ret]

rule bwa_mem:
    input:
        fastq = getFastq,
        bwa_index = expand("ref_files/{ref}/bwa_indices/{ref}/{ref}.fa.bwt", ref=reference),
    output:
        temp("analysis/preprocessed_files/align/{sample}/{sample}.bam")
    params:
        index = expand("ref_files/{ref}/bwa_indices/{ref}/{ref}.fa", ref=reference),
        threads = config['thread']
    threads: _align_min_threads
    message: "ALIGN: Running BWA mem for alignment"
    log: "analysis/logs/align.log"
    shell:
        "bwa mem -t {params.threads} {params.index}  {input.fastq} | samtools view -Sb - > {output} 2>>{log}"

rule uniquely_mapped_reads:
    """Get the uniquely mapped reads"""
    input:
        #"analysis/align/{sample}/{sample}.bam"
        getBam
    output:
        temp("analysis/preprocessed_files/align/{sample}/{sample}_unique.bam")
    message: "ALIGN: Filtering for uniquely mapped reads"
    log: "analysis/logs/align.log"
    threads: 8
    shell:
        #NOTE: this is the generally accepted way of doing this as multiply 
        #mapped reads have a Quality score of 0
        #NOTE: -@ = --threads
        "samtools view -bq 1 -@ {threads} {input} > {output}"


rule sortUniqueBams:
    """General sort rule--take a bam {filename}.bam and
    output {filename}.sorted.bam"""
    input:
        "analysis/preprocessed_files/align/{sample}/{sample}_unique.bam"
    output:
        #CANNOT temp this b/c it's used by qdnaseq!
        #bam = "analysis/preprocessed_files/align/{sample}/{sample}_unique.sorted.unfilterd.bam",
        bam = "analysis/preprocessed_files/align/{sample}/{sample}.sorted.bam",
        #"analysis/align/{sample}/{sample}_unique.sorted.bam.bai"
    message: "ALIGN: sort bam file"
    log: "analysis/logs/align.log"
    threads: 8
    shell:
        "sambamba sort {input} -o {output.bam} -t {threads} && samtools index {output} 2>>{log}"




#MODULE: PEAK CALLING using macs2

#PARAMETERS
_logfile="analysis/logs/peaks.log"
_macs_fdr="0.01"
_macs_keepdup="1"
_macs_extsize="146"

def getTreats(wildcards):
    tmp=[]
    #print(wildcards)
    rep_s = wildcards.rep
    rep_n = int(rep_s[-1])
    #print(rep_n)

    #USE the formula: treatment = 2^(rep-1); control = treatment+1
    treat = 2**(rep_n-1) if rep_n > 1 else 0
    r = config['runs'][wildcards.run]
    #print(r)
    #print(treat)
    if treat < len(r) and r[treat]:
        tmp = ["analysis/preprocessed_files/align/%s/%s.sorted.bam" % (r[treat],r[treat])]
    #print("TREAT: %s" % tmp)
    if not tmp:
        #NOTE: I can't figure out a proper kill command so I'll do this
        tmp=["ERROR! Missing treatment file for run: %s, rep: %s" % (wildcards.run, rep_s)]
    return tmp

def getConts(wildcards):
    tmp=[]
    #print(wildcards)
    rep_s = wildcards.rep
    rep_n = int(rep_s[-1])

    #USE the formula: treatment = 2^(rep-1); control = treatment+1
    cont = 2**(rep_n-1) + 1 if rep_n > 1 else 1
    r = config['runs'][wildcards.run]
    #print(r)
    #print(cont)
    if cont < len(r) and r[cont]:
        tmp = ["analysis/preprocessed_files/align/%s/%s.sorted.bam" % (r[cont],r[cont])]
    #print("CONT: %s" % tmp)
    return tmp


_reps = {}
for run in config['runs'].keys():
    r = config['runs'][run]
    tmp = []
    for (rep, i) in enumerate(range(0, len(r), 2)):
        if r[i]: tmp.append("rep%s" % str(rep+1))
    _reps[run] = tmp


class RepTemplate(Template):
    idpattern = r'[a-z][a-z0-9]*'


def _getRepInput(temp, suffix=""):
    """generalized input fn to get the replicate files
    CALLER passes in temp: a python string template that has the var runRep
    e.g. analysis/ceas/$runRep/$runRep_DHS_stats.txt
    Return: list of the string template filled with the correct runRep names
    """
    #print(temp)
    s = RepTemplate(temp)
    ls = []
    for run in config['runs'].keys():
        for rep in _reps[run]:
            #GENERATE Run name: concat the run and rep name
            runRep = "%s.%s" % (run, rep)
            ls.append(s.substitute(runRep=runRep,))
    #print(ls)
    return ls


def checkBAMPE(wildcards):
    """Fn returns '-f BAMPE' IF the run's FIRST treatment replicate (sample) is
    Paired-END.
    NOTE: this flag is required for macs2 callpeak, AUTO detect format does not
    work with PE bams!
    """
    r = config['runs'][wildcards.run]
    #GET first treatement sample
    first = config['fastq'][r[0]]
    ret = "-f BAMPE" if len(first) == 2 else ""
    return ret


rule macs2_callpeaks:
    input:
        treat=getTreats,
        cont=getConts,
    output:
        "analysis/preprocessed_files/peaks/{run}.{rep}/{run}.{rep}_peaks.narrowPeak",
        "analysis/preprocessed_files/peaks/{run}.{rep}/{run}.{rep}_summits.bed",
        temp("analysis/preprocessed_files/peaks/{run}.{rep}/{run}.{rep}_treat_pileup.bdg"),
        temp("analysis/preprocessed_files/peaks/{run}.{rep}/{run}.{rep}_control_lambda.bdg"),
    params:
        fdr=_macs_fdr,
        keepdup=_macs_keepdup,
        extsize=_macs_extsize,
        genome_size=config['genome_size'],
        outdir="analysis/preprocessed_files/peaks/{run}.{rep}/",
        name="{run}.{rep}",
        #handle PE alignments--need to add -f BAMPE to macs2 callpeaks
        BAMPE = lambda wildcards: checkBAMPE(wildcards),
        pypath="PYTHONPATH=%s" % config["python2_pythonpath"],
    message: "PEAKS: calling peaks with macs2"
    log:_logfile
    run:
        treatment = "-t %s" % input.treat if input.treat else "",
        control = "-c %s" % input.cont if input.cont else "",        
        shell("{params.pypath} {config[macs2_path]} callpeak --SPMR -B -q {params.fdr} --keep-dup {params.keepdup} -g {params.genome_size} {params.BAMPE} --extsize {params.extsize} --nomodel {treatment} {control} --outdir {params.outdir} -n {params.name} 2>>{log}")


rule get_bed:
    input:
        "analysis/preprocessed_files/peaks/{run}.rep1/{run}.rep1_peaks.narrowPeak"
    output:
        "analysis/preprocessed_files/bed/original_bed/{run}.bed"
    shell:
        "cp {input} {output}"


rule sortBedgraphs:
    """Sort bed graphs--typically useful for converting bdg to bw"""
    input:
        "analysis/preprocessed_files/peaks/{run}.{rep}/{run}.{rep}_treat_pileup.bdg"
    output:
        "analysis/preprocessed_files/peaks/{run}.{rep}/{run}.{rep}.sorted.bdg"
    params:
        #msg just for message below
        msg= lambda wildcards: "%s.%s" % (wildcards.run, wildcards.rep)
    message: "PEAKS: sorting bdg pileups {params.msg}"
    log:_logfile
    shell:
        "sort -k1,1 -k2,2n {input} > {output} 2>>{log}"

rule bdgToBw:
    """Convert bedGraphs to BigWig"""
    input:
        bdg = "analysis/preprocessed_files/peaks/{run}.{rep}/{run}.{rep}.sorted.bdg",
        chroms = expand("ref_files/{ref}/{ref}.len", ref=reference)
    output:
        bw = "analysis/preprocessed_files/peaks/{run}.{rep}/{run}.bw",
    params:
        chroms=config['chrom_lens'],
        #msg just for message below
        msg= lambda wildcards: "%s.%s" % (wildcards.run, wildcards.rep)
    message: "PEAKS: Convert bedGraphs to BigWig {params.msg}"
    log:_logfile
    shell:
        "bedGraphToBigWig {input.bdg} {input.chroms} {output.bw} 2>>{log}"
    
rule get_bigwig:
    input: 
        "analysis/preprocessed_files/peaks/{run}.rep1/{run}.bw"
    output:
        "analysis/preprocessed_files/bigwig/{run}.bw"
    shell:
        "cp {input} {output}"



rule generate_IGV_session:
    """Generates analysis/peaks/all_treatments.igv.xml, a igv session of all
    of the treatment.bw files"""
    input:
        _getRepInput("analysis/preprocessed_files/bigwig/$runRep/$runRep_treat_pileup.bw")
