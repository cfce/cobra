#!/usr/bin/env python

# vim: syntax=python tabstop=4 expandtab

# @AUTHOR:Xintao Qiu
# @Email: Xintao_Qiu@hotmail.com
# @Date: July,18,2016

import argparse
import sys
import pandas as pd
import os.path
import re
import numpy as np

def parseArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--rpkmthreshold', required=True, nargs='*', help="Threshold for RPKM")
    parser.add_argument('-f','--countfile', required=True, nargs='*', help="Provide RPKM count file.")
    parser.add_argument('-n','--samplenumber', required=True, nargs='*', help="Provide number of samples need to apply the RPKM threshold.")
    args = parser.parse_args()
    return args


#parse the arguments
args = parseArgs()


#Read raw count data 
countfile=pd.read_csv(args.countfile[0],index_col=None)

#Obtain the rows that all RPKM for samples are larger than threshold
select = (countfile.iloc[:, 4:] >= float(args.rpkmthreshold[0])).sum(axis=1) >= float(args.samplenumber[0])

main_df=countfile.loc[select, :]

print(main_df.to_csv(header=True, index=False))
