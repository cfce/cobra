from __future__ import print_function
import os
import sys
import re
import base64
import numpy
import math
import argparse
import pandas



#July 2019
#Convert homer output Position Weight MAtrix (PWM) to motifs, cluster motifs, and display in HTML

class Motif:
    """
    Class for sequence motifs.
    """
    _ATTRIBUTES = ['id', 'pssm', 'seqpos_results', 'antisense',
                   'source', 'sourcefile', 'species', 'fullname', 'pmid',
                   'numseqs', 'factors', 'entrezs', 'refseqs', 'dbd']
    _results_fields = ['numhits','cutoff','zscore','meanposition','pvalue']

    #maybe i'll make pssm the only required attribute
    def __init__(self):
        """initializes all attributes to None"""
        for attr in Motif._ATTRIBUTES:
            setattr(self, attr, None)
        #set the seqpos_result field to None
        #and convenience accessors/setters, e.g. self.getpvalue
        self.seqpos_results = {}
        for attr in Motif._results_fields:
            self.seqpos_results[attr] = None
            def curry(x):
                return lambda : self.seqpos_results[x]
            #setattr(self, "get"+attr, lambda : self.seqpos_results[attr])
            setattr(self, "get"+attr, curry(attr))
            #setattr(self, "set"+attr, lambda x: self.seqpos_results[attr] = x)

                
    @staticmethod
    def _validpssm(pssm):
        """Checks to see if its a valid pssm:
        1. each row must have 4 cols of floats
        2. each row must sum to 1.0
        """
        def feq(f1, f2):
            epsilon = 1e-6
            return abs(f1 - f2) < epsilon

        if pssm is None: raise BadPssm("None is not a valid pssm")
        if len(pssm) == 0: raise BadPssm("Empty pssm")
        
        for i, row in enumerate(pssm):
            if len(row) != 4:
                raise BadPssm("row %s is not length 4" % i)
            else:
                if not feq(sum(row), 1.0):
                    print(i, sum(row))
                    raise BadPssm("row %s sums to %s NOT 1.0" % (i, sum(row)))
        return True
                
    def setpssm(self, pssm):
        """The preferred method to set the motif's pssm. does error checking"""
        if Motif._validpssm(pssm):
            self.pssm = pssm

    def __str__(self):
        """How motifs should be printed out"""
        divider = "".join(["-" for x in range(79)])
        lines = ["%s: %s" % (attr,getattr(self, attr)) \
                 for attr in Motif._ATTRIBUTES]
        return "%s\n%s\n%s" % (divider, "\n".join(lines), divider)

    
class MotifList(list):
    """Class to hold a list of unique motifs, and perform operations over them
    """
        
    def append(self, motif_obj):
        """Tries to append motif_obj to the end of the list;
        IF duplicate: raises DuplicateMotif exception
        """
        if motif_obj not in self:
            #note if you do self.append, that's infinite recursion
            unbnd_app = list.append
            unbnd_app(self, motif_obj)
        else:
            raise DuplicateMotif


class Cluster:
   def __init__(self):
       self.motif = {}
       self.nodes = []
       self.nodescount = 1
       self.score_for_node = 0.0


def sum_IC(m1):
    """sum of IC of each column in the matrix
    """
    ic = [IC(t) for t in m1]
    return sum(ic)


def IC(v1):
    """IC of a vector
    """
    total = sum(v1)
    v1 = [t * 1.0 / total for t in v1]

    return 2 + sum([t * math.log(t, 2) for t in v1])


def pcc_vector(v1, v2):
    """Pearson Correlation Coefficient for 2 vectors
    """
    len1 = len(v1)
    len2 = len(v2)
    if len1 != len2:
        return None
    else:
        length = len1
    avg1 = 1.0 * sum(v1) / len(v1)
    avg2 = 1.0 * sum(v2) / len(v2)
    dxy = [(v1[i] - avg1) * (v2[i] - avg2) for i in range(length)]
    dx2 = [(v1[i] - avg1) ** 2 for i in range(length)]
    dy2 = [(v2[i] - avg2) ** 2 for i in range(length)]
    return sum(dxy) / (sum(dx2) * sum(dy2)) ** 0.5


def pcc_matrix_IC(m1, m2):
    """IC weighted pcc score of each column, and get a sum
    
    The sharp of m1 and m2 should be same
    """
    if len(m1) != len(m2):
        return None
    weighted_pcc = 0
    for pos in range(len(m1)):
        ic_x = IC(m1[pos])
        ic_y = IC(m2[pos])
        #print ic_x, ic_y
        ic_xy = (ic_x * ic_y) ** 0.5
        pcc = pcc_vector(m1[pos], m2[pos])
        
        #if ic_x >= 0.2 and ic_y >= 0.2:
        #    ic = ic_xy
        #elif ic_x >= 0.2:
        #    ic = ic_x
        #elif ic_y >= 0.2:
        #    ic = ic_y
        #else:
        #    ic = 0
        #weighted_pcc += ic * pcc
        weighted_pcc += ic_xy * pcc
    return weighted_pcc


def flat(cluster):
    '''flat a Cluster class into a list.
    '''
    if not cluster.nodes:
        return [cluster.motif]
    else:
        return flat(cluster.nodes[0]) + flat(cluster.nodes[1])


def similarity(m1, m2):
    len1 = len(m1)
    len2 = len(m2)
    if len1 < len2:
        m1, m2 = m2, m1 #ensure m1 > m2
        len1, len2 = len2, len1
        #print range(0, len2 - len1 + 1)
    max_score = 0
    shift_pos = None
    reverse = None
    for shift in range(1-len2, len1):
        if shift < 0:
            m1new = m1[:len2+shift]
            m2new = m2[-shift:]
            m2rev = [t[::-1] for t in m2[::-1]][-shift:] # important: reverse first, then slice
        elif shift <= len1 - len2:
            m1new = m1[shift:shift+len2]
            m2new = m2[:]
            m2rev = [t[::-1] for t in m2[::-1]][:]
        elif len1 - shift < len2:
            m1new = m1[shift:]
            m2new = m2[:len1-shift]
            m2rev = [t[::-1] for t in m2[::-1]][:len1-shift]
        
        ic1new = [IC(t) for t in m1new]
        ic2new = [IC(t) for t in m2new]
        ic2rev = [IC(t) for t in m2rev]
        
        # un-reverse
        weight_overlap = sum([(t1 * t2) ** 0.5 for t1, t2 in zip(ic1new, ic2new)])
        weight_nonoverlap = sum_IC(m1) + sum_IC(m2) - sum_IC(m1new) - sum_IC(m2new)
        weighted_pcc = pcc_matrix_IC(m1new, m2new)
        score = weighted_pcc / (weight_overlap + weight_nonoverlap)
        if score > max_score:
            max_score = score
            shift_pos = shift
            reverse = False
            
        # reverse
        weight_overlap = sum([(t1 * t2) ** 0.5 for t1, t2 in zip(ic1new, ic2rev)])
        weight_nonoverlap = sum_IC(m1) + sum_IC(m2) - sum_IC(m1new) - sum_IC(m2rev)
        weighted_pcc = pcc_matrix_IC(m1new, m2rev)
        score = weighted_pcc / (weight_overlap + weight_nonoverlap)
        if score > max_score:
            max_score = score
            shift_pos = shift
            reverse = True
    
    return max_score, shift_pos, reverse


def motif_hcluster2(motif_list, cutoff):
    """Use complete distance, that mean get farest for each cluster.
    
    This one is for the MDSeqPos.py to use, input motif_list is list of Motif() obj.
    """
    clusters = [] # a list with clustered motifs.
    cluster_score_mat = numpy.array([[None for t in range(len(motif_list))] for m in range(len(motif_list))]) # 2d matrix, order as keys.
    
    # Adding motifs from mp to clusters
    for m in motif_list:
        cl = Cluster()
        cl.motif = m
        clusters.append(cl)

    #Calc each 2 pair of motifs and fill (score, position, strand) in matrix like below
    # xxx
    #  xx
    #   x
    #    
    for i in range(len(clusters)-1):
        for j in range(i+1, len(clusters)):
            ts = similarity(clusters[i].motif.pssm, clusters[j].motif.pssm) # score, position, strand
            cluster_score_mat[i][j] = ts[0] # 1-distance score

    idcount = 0
    mround = 0
    
    #find index of the max score
    while 1:
        if not cluster_score_mat.any():
            break
        max_score = cluster_score_mat.max()
        mshape = cluster_score_mat.shape
        max_index = (999, 999)
        findit = False
        for i in range(mshape[0]):
            for j in range(i+1, mshape[1]):
                if abs(max_score - cluster_score_mat[i][j]) < 1e-5:
                    max_index = (i, j)
                    findit = True
                    break
            if findit:
                break
        
        # if the max is smaller than cutoff, end of clustering
        if max_score < cutoff:
            break
        
        # Build merged motif except the matrix, matrix will add later.
        max2 = clusters.pop(max_index[1])
        max1 = clusters.pop(max_index[0])
        merged = Cluster()
        merged.motif = Motif()
        merged.score_for_node = max_score
        merged.nodescount = max1.nodescount + max2.nodescount
        merged.nodes = [max1,max2]
        merged.motif.id = 'MT%d'%idcount
        
        idcount += 1
        
        # Append merged motif to cluster
        clusters.append(merged)
        print('Clustering, finish round '+ str(mround))
        mround += 1
        
        # Refine score matrix and list. Including cut 2 line and 2 col, and then add 1 line and 1 col.
        i, j = max_index
        score_y1 = numpy.append(cluster_score_mat[:,i][:i], cluster_score_mat[i][i:])
        score_y2 = numpy.append(cluster_score_mat[:,j][:j], cluster_score_mat[j][j:])
        score_ymin = []
        for x1, x2 in zip(score_y1, score_y2):
            if x1 is None or x2 is None:
                pass
            elif x1 > x2:
                score_ymin.append(x2)
            else:
                score_ymin.append(x1)
        cluster_score_mat = numpy.append(cluster_score_mat[:j], cluster_score_mat[j+1:],0)
        cluster_score_mat = numpy.append(cluster_score_mat[:i], cluster_score_mat[i+1:],0)
        cluster_score_mat = numpy.append(cluster_score_mat[:,:j], cluster_score_mat[:,j+1:], 1)
        cluster_score_mat = numpy.append(cluster_score_mat[:,:i], cluster_score_mat[:,i+1:], 1)
        x = cluster_score_mat.shape[0]
        
        if score_ymin:
            cluster_score_mat = numpy.append(cluster_score_mat, [[t] for t in score_ymin], 1)
            cluster_score_mat = numpy.append(cluster_score_mat, [[None for t in range(x+1)]], 0)
        else:
            break # all motif in it are 1 cluster.
    
    #print 'Cluster %d motifs into %d clusters' %(len(motif_list), len(clusters))
    flat_clusters = [flat(t) for t in clusters]

    return flat_clusters


def convertTable(table):
    """
    Convert table of numbers to Format specified in Motif.from_flat_file()
    """
    height = 1
    f = open(table,"r")

    pssm = [[0 for x in range(4)] for y in range(height)]

    header = f.readline()
    items = header.split()
    values = items.pop(-1).split(',')

    if("Multiplicity" in values[-1]):
        values = items.pop(-1).split(',')

    for i, val in enumerate(values):
        if(i<=1):
            num = val[2:val.find("(")]
            per = val[val.find("(")+1:val.find(")")]
            items.append(num)
            items.append(per)
        else:
            items.append(val[2:])     #Get rid of beginning tag

    #Items is in form [Genome, Name, number, log(pvalue), QValue, TValue, TPercent, BValue, BPercent, PValue]
    newItems = [None]*7
    newItems[0] = items[1]
    newItems[1] = items[9]
    newItems[2] = items[3]
    newItems[3] = items[5]
    newItems[4] = items[6]
    newItems[5] = items[7]
    newItems[6] = items[8]
    #newItems is in form[Name, PValue, log(PValue), TValue, TPercent, BValue, BPercent] 

    for i, line in enumerate(f):
        #Not sure how many lines to expect, so the number of rows expands
        if(line[0] == ">" and len(pssm)>1):
            break;
        if(i == len(pssm)):
            pssm.append([0,0,0,0])
        nums = line.strip().split('\t')
        #for collumn in row
        for j in range(3):
            try:
                pssm[i][j] = float(nums[j])
            except:
                print("error")    # debug
        #calculate last value, because rounding errors were messing with row sums
        pssm[i][-1] = 1.0 - (pssm[i][0] + pssm[i][1] + pssm[i][2]) 
        if(pssm[i][-1] == 0.0):
            pssm[i][-1] = .001
            pssm[i][-2] -= .001

    f.close()
    return [pssm, newItems]


def convertDirectory(path):
    """
    convert directory of .motif and .logo.png files to array of data
    """
    filenames = []
    i=0
    files = os.listdir(path)
    print("current input file path is " + path)
    for file in files:
        if(file.endswith(".motif") and ("similar" not in file) and "RV" not in file):
            i+=1
            #create name of file based on number of motif files
            # this creates a list of file names that is in order numerically
            file = 'known' + str(i) + '.motif'
            data = convertTable(path+file)
            for f in filenames:
                #check for duplicate names and differentiate
                if(f[1][0] == data[1][0]):
                    if (data[1][0])[-1].isdigit():
                        data[1][0] = data[1][0] + str(int((data[1][0])[-1])+1)
                    else:
                        data[1][0] = data[1][0]+"1"
                    break;
            #check for misformatted motif files
            if(data[1][0] != "SeqBias:"):
                filenames.append(data)
    
    #Calculate total sequences using percentage of sequences for first motif
    if(len(filenames) == 0):
        targetSeq = 0
        backSeq = 0
    else:
        targetSeq = int(round(100.0/float((filenames[-1][1][4])[:-1])*float(filenames[-1][1][3])))
        backSeq = int(round(100.0/float((filenames[-1][1][6])[:-1])*float(filenames[-1][1][5])))
    return filenames, [targetSeq, backSeq]


def createMotifList(files):
    """
    create list of Motifs from data array
    """
    mlist = MotifList() 
    for pack in files:
        motif = Motif()
        table = pack.pop(0)
        pack = pack[0]
        motif.setpssm(table)
        motif.id = pack[0]
        motif.seqpos_results["pvalue"] = pack[1]
        mlist.append(motif)
    

    return mlist, files

def sortByIndex(array, i):
    #print(array)
    #print(i)
    #print(type(array))
    #print(len(array))
    
    tmp = []
    for j in range(len(array)):
        y = array[j][i]
        pd = (y)[y.find('-')+1:]
        try:
            float(pd)
            tmp.append(array[j])
        except ValueError:
            print("Not a float")
    #tmp = numpy.array(tmp)
    #print(type(tmp))
    #print(len(tmp))
    tmp.sort(key=lambda x: -1* float((x[i])[x[i].find('-')+1:]))
    return tmp


def toHtml(outName, motifs,  numseq, sig):
    #Set up HTML Page
    headers = ["Cluster", "Motif","Name", "PValue", "log(PValue)", "# Target Sequences with Motif", "%"+" of Targets Sequences with Motif", "# Background Sequences with Motif", "%"+" of Background Sequences with Motif"]
    htmlFile = open(outName.strip(), "w")
    htmlFile.write("<html>\n<head>\n<title>Motif Output</title><style>table, th, td {border: 1px solid black;border-collapse: collapse;}</style>\n</head>\n<body>\n")
    htmlFile.write("<h1>Homer Known Motif Enrichment Results</h1>")

    htmlFile.write("<br>Total Target Sequences = ")
    htmlFile.write(str(numseq[0]))
    htmlFile.write(", Total Background Sequences = ")
    htmlFile.write(str(numseq[1]))
    htmlFile.write("</br>")
    htmlFile.write('<table>\n')

    #Write headers to table
    htmlFile.write('<tr style="border-bottom:4px solid black;">\n')
    for lab in headers:
        htmlFile.write('<th>'+str(lab)+"</th>")
    htmlFile.write("</tr>\n")
    
    for j,c in enumerate(motifs):
        #Set up cluster numbering
        if sig:
            htmlFile.write('<tr style ="border-bottom:4px solid black;">\n<th scope ="rowgroup">'+str(j+1)+'</th>')
        else:
            htmlFile.write('<tr>\n<th style="border-bottom:4px solid black;" rowspan="'+str(len(c))+'" scope ="rowgroup">'+str(j+1)+'</th>')

        #Write data to table
        for i, m in enumerate(c):
            last = i == len(c)-1
            if i!=0:
                htmlFile.write("<tr>\n")
            if last:
                htmlFile.write('<td style ="border-bottom:4px solid black;"><img src="data:image/png;base64,'+m.pop(0)+'"></td>')
            else:
                htmlFile.write('<td><img src="data:image/png;base64,'+m.pop(0)+'"></td>')
            #for attribute in each motif
            for k in range(len(m)):
                if last:
                    htmlFile.write('<td style ="border-bottom:4px solid black;" align="center">'+str(m[k])+"</td>")
                else:
                    htmlFile.write('<td align="center">'+str(m[k])+"</td>")
            htmlFile.write("</tr>\n")
            #if in significant mode, only write first motif of each cluster
            if sig:
                break
            
    htmlFile.write("</table>")
    htmlFile.write("</html>")
    htmlFile.close()


def getArgs():
    """
    create argument parser for terminal calls
    Usage: python2.7 html_display.py -a Target_Directory -d Distance_Cutoff -s -o Output_File
    Note: Significant mode is inactive unless -s is present
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", action = 'store', default = "knownResults/", help='Target Directory')
    parser.add_argument("-d", action = 'store', default = .7, type = float, help = 'Distance Cutoff for Clustering (Default = 0.7)')
    parser.add_argument('-s', dest='sig', action='store_true', help = 'Only include the first motif of every cluster (default = False)')
    parser.set_defaults(sig=False)
    parser.add_argument('-o', action = 'store', default = "Motif_Output.html", help = 'Output File')
    parser.add_argument('-c', action='store', default="Motif_Output.csv", help='Output CSV File')
    args = parser.parse_args()

    return args.a, args.d, args.o, args.sig, args.c


def clusterData(cluster, motifs, tocsvName):
    """
    Create a list of motif filedata to mimic the format motif cluster
    """
    clusteredMotifs=[]
    for c in cluster:
        motifsToAdd=[]
        matched = 0
        for ele in c:
            for m in motifs:
                if ele.id == m[1]:
                    matched +=1
                    motifsToAdd.append(m)
            if matched>=len(c):
                break;
        
        motifsToAdd = sortByIndex(motifsToAdd, 2)
        clusteredMotifs.append(motifsToAdd)
    tmp = []
    """
    for motif in clusteredMotifs:
        print("XXXXXXXXX")
        for thing in motif:
            print("YYYYYYYYYYYY")
            print(thing)
            """
    for j in range(len(clusteredMotifs)):
        try:     
            y = clusteredMotifs[j][0][2]
            pd = (y)[y.find('-')+1:]
            float(pd)
            tmp.append(clusteredMotifs[j])
        except:
            print("failed on motif #" +str(j) + ". Length = " + str(len(clusteredMotifs[j])))


    tmp.sort(key=lambda x: -1* float((((x[0])[2])[(x[0])[2].find("-")+1:])))

    ### print a csv file & change tmp to the original format
    tmp2 = []
    for i in range(len(tmp)):
        tmp2.append(tmp[i][0])

    tmp2 = pandas.DataFrame(tmp2, columns=["Motif", "Name", "PValue", "log(PValue)", "# Target Sequences with Motif",
                                           "%" + " of Targets Sequences with Motif",
                                           "# Background Sequences with Motif",
                                           "%" + " of Background Sequences with Motif", "filename"])
    tmp2.to_csv(tocsvName)

    for i in range(len(tmp)):
        for j in range(len(tmp[i])):
            tmp[i][j].pop()

    return tmp


def main():
    knownDir, distCutoff, outName, significant, csvName = getArgs()

    if(knownDir[-1]!='/'):
        knownDir+='/'

    #try:
        #convert directory of .motif files to data array
    filenames, sequences = convertDirectory(knownDir)
    #except:
    #    print("Warning: Cannot convert directory, too few motifs")
    #    open(outName, 'a').close()
    #    sys.exit()
    mlist, filenames = createMotifList(filenames)

    
    filedata = []
    for file in filenames:
        file = file[0]
        filedata.append(file)
    
    #sort by p value
    #filedata = sortByIndex(filedata, 1)
    
    #logos are sor
    # ted by p value, smallest first
    #logos are encoded in Base 64, so that they can be imbedded directly in the html file
    for i, file in enumerate(filedata):
        try:
            #known format
            filename = knownDir+"known"+str(i+1)+".logo.png"
            encoded = base64.b64encode(open(filename, "rb").read())
        except:
        #   #de novo format
            filename = knownDir+"motif"+str(i+1)+".logo.png"
            encoded = base64.b64encode(open(filename, "rb").read())
        #Base 64 encoding is included in the filedata
        file.insert(0, encoded)
        file.append(filename)
    

    #pre-existing code to cluster Motifs based on pssm
    flat_clusters = motif_hcluster2(mlist, distCutoff)
    #Cluster motif data array to match Motif object clustering
    filedata = clusterData(flat_clusters, filedata, csvName)
    toHtml(outName, filedata, sequences, significant)

if __name__ == "__main__":
    main()
