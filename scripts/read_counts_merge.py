#!/usr/bin/env python

# vim: syntax=python tabstop=4 expandtab

# @AUTHOR:Xintao Qiu
# @Email: Xintao_Qiu@hotmail.com
# @Date: July,18,2016

import argparse
import sys
import pandas as pd
import os.path
import re

def parseArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s','--sampleorder',required=True,nargs='*', help="Provide ordered sample names")
    parser.add_argument('-f','--countfiles', required=True, nargs='*', help="Provide count filename (*.count). Multiple filenames can be given as space separated values.")
    parser.add_argument('-t','--total_count', required=True, nargs='*', help="Provide total count filename (*.total_count). Multiple filenames can be given as space separated values.")
    args = parser.parse_args()
    return args

#parse the arguments
args = parseArgs()

#store the big count file
main_df = pd.DataFrame()

#Extract the first four cols from a count file
countfile=pd.read_table(args.countfiles[0], header=None, index_col=False)
main_df["chrom"]=countfile.iloc[:, 0]
main_df["chromStart"]=countfile.iloc[:, 1]
main_df["chromEnd"]=countfile.iloc[:, 2]
main_df["name"]=countfile.iloc[:, 3]

#Read sample order from matasheet, use the frist col as index (sample name)
metadata = pd.read_table(args.sampleorder[0], index_col=0, sep=',') #metadata.index is the frist col

#match the count file with the filenames and order in metafile
for samplename in metadata.index:
   for path in args.countfiles:
        filename=os.path.basename(path)
        #print(os.path.splitext(filename)[0])
        if (samplename == os.path.splitext(filename)[0]):
          for tpath in args.total_count:
             tfilename=os.path.basename(tpath)
             if (samplename == os.path.splitext(tfilename)[0]):
                  df = pd.read_table(path, header=None, index_col=False)
                  tdf = pd.read_table(tpath, header=None, index_col=False)
                  total_count=int(tdf.iloc[4, :].to_string().split()[1])
                  RPK=df.iloc[:, len(df.columns)-1]/((main_df["chromEnd"] - main_df["chromStart"])/1000)
                  #RPKM
                  main_df[samplename]=RPK/(total_count/1000000)

print(main_df.to_csv(header=True, index=False))
