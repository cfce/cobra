#!/usr/bin/env python

# vim: syntax=python tabstop=4 expandtab

# @AUTHOR:Xintao Qiu
# @Email: Xintao_Qiu@hotmail.com
# @Date: July,18,2016

import argparse
import sys
import pandas as pd
import os.path
import re
import numpy as np


def parseArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument('-o','--selectopt',required=True,nargs='*',help="Option for select features #sd- Standard deviation #cov- Coefficient of Variation #av- mean.")
    parser.add_argument('-p','--percent',required=True,nargs='*',help="Percent for cutoff.")
    parser.add_argument('-f','--countfile', required=True, nargs='*', help="Provide count output file.")
    args = parser.parse_args()
    return args

#parse the arguments
args = parseArgs()


#read raw count data 
countfile=pd.read_csv(args.countfile[0],index_col=None)

#an empty output file
main_df = pd.DataFrame()

#sd -standard deviation
if(args.selectopt[0] == "sd"):
   countfile["std"]=countfile.iloc[:,4:].std(axis=1)
   cutoff = countfile["std"].quantile(1-(int(args.percent[0])/100))
   main_df=countfile.loc[countfile['std'] >= cutoff]
   
#cov -coefficient of variation
elif(args.selectopt[0] == "cov"):
   std = countfile.iloc[:,4:].std(axis=1)
   mean = countfile.iloc[:,4:].mean(axis=1)
   countfile["cov"] = abs(std/mean)
   cutoff = countfile["cov"].quantile(1-(int(args.percent[0])/100))
   main_df=countfile.loc[countfile['cov'] >= cutoff]

#av -mean
elif(args.selectopt[0]=="av"):
   countfile["avg"]=countfile.iloc[:,4:].mean(axis=1)
   cutoff = countfile["avg"].quantile(1-(int(args.percent[0])/100))
   main_df=countfile.loc[countfile['avg'] >= cutoff]

print(main_df.to_csv(header=True, index=False))
