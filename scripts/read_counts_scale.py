#!/usr/bin/env python

# vim: syntax=python tabstop=4 expandtab

# @AUTHOR:Xintao Qiu
# @Email: Xintao_Qiu@hotmail.com
# @Date: July,18,2016

import argparse
import sys
import pandas as pd
import os.path
import re
import numpy as np
import readline
import rpy2.robjects as robjects
from rpy2.robjects.conversion import localconverter
from rpy2.robjects.packages import importr
from rpy2.robjects import pandas2ri

def parseArgs():
    parser = argparse.ArgumentParser()
    parser.add_argument('-s','--scale',required=True,nargs='*',help="Scale method for the nomalize counts among samples z- z-score; q- quantile-normalize; l- log-transform.")
    parser.add_argument('-f','--countfile', required=True, nargs='*', help="Provide count output file.")
    args = parser.parse_args()
    return args


#parse the arguments
args = parseArgs()


#Read raw count data 
countfile=pd.read_csv(args.countfile[0],index_col=None)

#Copy the orgiginal file to output
main_df=countfile

#zscore
if(args.scale[0] == "z"):
   for index,row in countfile.iloc[:,4:].iterrows():
       main_df.iloc[index,4:]=(row - np.mean(row))/np.std(row,ddof=1)

#quantile-normalize
elif(args.scale[0] == "q"):

   #Transform the counts file to Matrix
   matrix = countfile.iloc[:,4:].values.transpose()

   #Matrix to Vector
   v = robjects.FloatVector([ element for col in matrix for element in col ])

   #Vector to r matrix
   m = robjects.r['matrix'](v, ncol = len(matrix))

   #Library the preprocessCore package
   preprocessCore = importr('preprocessCore')

   #Quantile Normalization
   Rnormalized_matrix = preprocessCore.normalize_quantiles(m)

   #Change the result to dataFrame
   with localconverter(robjects.default_converter + pandas2ri.converter):
        main_df.iloc[:,4:]=robjects.conversion.rpy2py(Rnormalized_matrix)

#log-transform
elif(args.scale[0]=="l"):
   main_df.iloc[:,4:]=np.log(1 + countfile.iloc[:,4:])

elif(args.scale[0]=="n"):
   main_df=countfile

print(main_df.to_csv(header=True, index=False))
