#!/bin/bash

echo -e "###cut start: " `date +%y-%m-%d.%H:%M:%S` "\n"

usage() { echo "Usage: $0 [-d <in>] [-o <out>] "; exit 1; }


while getopts :d:o: OPTION
do
    case $OPTION in
        d)
            in="$OPTARG"
            ;;
	o)
	    out="$OPTARG"
            ;;			
        \?)
            usage
            ;;
    esac
done

echo $in,  $out

paste <(cut -f1 $in | tr -s '_' '\t') <(cat $in | rev | cut -f1 | rev) | tail -n +2 | awk '{print $1"\t"$2"\t"$3 > "'$out'"$4".bed"}'
